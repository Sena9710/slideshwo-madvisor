<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
          $table->increments('id');
          $table->string('firstname');
          $table->string('lastname');
          $table->enum('sex', ['0', '1']);
          $table->string('city');
          $table->string('province');
          $table->string('zipcode');
          $table->string('street');
          $table->timestamp('dob');
          $table->string('email')->unique();
          $table->string('cellphone');
          $table->string('password');
          $table->enum('type', ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18']);
          $table->string('website');
          $table->string('contact');
          $table->string('geographical');
          $table->string('sector');
          $table->string('investment');
          $table->string('minimum');
          $table->string('maximum');
          $table->string('avg');
          $table->integer('executives');
          $table->string('funds');
          $table->string('capitals');
          $table->string('companies');
          $table->string('exits');
          $table->string('avg_funds');
          $table->rememberToken();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('partners');
    }
}
