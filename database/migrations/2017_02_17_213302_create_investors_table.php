<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->enum('sex', ['0', '1']);
            $table->string('city');
            $table->string('province');
            $table->string('zipcode');
            $table->string('street');
            $table->timestamp('dob');
            $table->string('email')->unique();
            $table->string('cellphone');
            $table->string('password');
            $table->integer('type');
            $table->string('capital');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('investors');
    }
}
