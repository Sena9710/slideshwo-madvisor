<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['-1', '0', '1'])->default('-1');
            $table->string('name');
            $table->string('street');
            $table->string('zipcode');
            $table->string('website');
            $table->string('referrer');
            $table->string('position');
            $table->string('email')->unique();
            $table->string('cellphone');
            $table->string('fax')->default('')->nullable();
            $table->string('password');
            $table->string('description')->default('');
            $table->string('product')->default('');
            $table->string('market')->default('');
            $table->string('status_development')->default('');
            $table->string('status')->default('');
            $table->string('property')->default('');
            $table->longtext('managment')->nullable();
            $table->string('model_business')->default('');
            $table->longtext('swot')->nullable();
            $table->string('financial_information')->default('');
            $table->enum('previous_investors', ['0', '1'])->default('0');
            $table->string('amount')->default('');
            $table->string('pre_money')->default('');
            $table->longtext('requirements')->nullable();
            $table->string('specifics')->default('');
            $table->string('why_us')->default('');
            $table->tinyInteger('verified')->default(0);
            $table->string('email_token')->nullable();
            $table->enum('executive_status', ['0', '1', '2'])->default('0');
            $table->string('executive_status_reason')->default('');
            $table->string('advisor')->default('');
            $table->string('vat')->default('');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
