<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntermediariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intermediaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type');
            $table->string('firstname');
            $table->string('lastname');
            $table->enum('sex', ['0', '1']);
            $table->string('company_email')->unique();
            $table->string('province');
            $table->string('cellphone')->default('');
            $table->string('email')->unique();
            $table->enum('employment_relationship', ['0', '1']);
            $table->string('network')->default('');
            $table->string('bank')->default('');
            $table->integer('manager_type');
            $table->string('experience');
            $table->string('username')->unique();
            $table->string('company_rank');
            $table->string('heritage')->default('');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('intermediaries');
    }
}
