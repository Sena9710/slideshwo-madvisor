<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvisorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advisors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->enum('sex', ['0', '1']);
            $table->string('city');
            $table->string('province');
            $table->string('zipcode');
            $table->string('street');
            $table->string('dob');
            $table->string('email')->unique();
            $table->string('cellphone');
            $table->string('password');
            $table->enum('type', ['0', '1', '2', '3']);
            $table->string('vat');
            $table->string('business_name');
            $table->enum('manager', ['0', '1'])->default('0');
            $table->tinyInteger('verified')->default(0);
            $table->string('email_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('advisors');
    }
}
