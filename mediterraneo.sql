/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : mediterraneo

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-02-22 00:57:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for companies
-- ----------------------------
DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('-1','0','1') COLLATE utf8mb4_unicode_ci DEFAULT '-1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referrer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cellphone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `product` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `market` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `status_development` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `property` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `managment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `model_business` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `swot` longtext COLLATE utf8mb4_unicode_ci,
  `financial_information` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `previous_investors` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `pre_money` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `requirements` longtext COLLATE utf8mb4_unicode_ci,
  `specifics` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `why_us` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `verified` tinyint(4) NOT NULL DEFAULT '0',
  `email_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `executive_status` enum('0','1','2') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `executive_status_reason` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `companies_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of companies
-- ----------------------------
INSERT INTO `companies` VALUES ('1', '-1', 'sdgsdf', 'largo campofiorito, 1', '90100', 'local.host', 'Cristian Cosenza', 'Nulla', 'cri16992@gmail.com', '3277796535', null, '$2y$10$4k3dCRYpk99VGAqX/ZtxFeNdoe1m9cjdapdzNwzGVxCuJal1YCtaS', '', '', '', '', '', '', '', '', null, '', '0', '', '', null, '', '', '1', null, null, null, null, '2017-02-20 16:55:46', '2017-02-20 16:56:32');
INSERT INTO `companies` VALUES ('2', '-1', 'sqfgfqdg', 'ffdgdfg', '90100', 'local.host', 'Cristian Cosenza', 'Nulla', 'cri16993@gmail.com', '3277796535', null, '$2y$10$bUTJWAGsO5JFyplLhtGVW.bjiJDEI2mnXhMyshLp.UKtn04zNhq5e', '', '', '', '', '', '', '', '', null, '', '0', '', '', null, '', '', '1', null, null, null, '6CxV1JgvuTMhUU43Na9axjGc71YATDVYYZib7RY82cTqFt0AbQzZg0ET7k3G', '2017-02-20 16:58:29', '2017-02-20 16:58:39');
INSERT INTO `companies` VALUES ('3', '1', 'hjklthpi', 'hgdghgfh', '90100', 'local.host', 'Cristian Cosenza', 'Nulla', 'cri1699@gmail.com', '3277796535', null, '$2y$10$TeC/OgDN7Zzpw3XS7.vwfOTtTtkUDjG7sbowVm7IlQRXHNXEPyHIO', 'fj', 'ghjgh', 'jghjghjgh', 'Altro|Pre-seed', 'ghjhg', 'ghjgh', 'hjgh', 'jghjgdhj', 'fhjghd|gdhjgh|jghj|ghjghjghj|jhlkhoyujyuy', '5-4-4-4-4|4-1-4-4-4|1-1-1-1-1|1-1-1-1-1$9----|----|----|----', '0', '76785', '756785', '5|0|ghfgh', 'fghfghf', 'hfghgfhgf', '1', null, '2', 'fai schifo', 'SpOuDgrvzjF7g8CxTa5LcsV390qkgEktdpHep9nL5oP8Ugq7sMGMRN77iPb3', '2017-02-20 16:59:31', '2017-02-20 19:53:42');
INSERT INTO `companies` VALUES ('4', '-1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, '', '0', '', '', null, '', '', '0', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for company_password_resets
-- ----------------------------
DROP TABLE IF EXISTS `company_password_resets`;
CREATE TABLE `company_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `company_password_resets_email_index` (`email`),
  KEY `company_password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of company_password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for intermediaries
-- ----------------------------
DROP TABLE IF EXISTS `intermediaries`;
CREATE TABLE `intermediaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cellphone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employment_relationship` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `network` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bank` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `manager_type` int(11) NOT NULL,
  `experience` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_rank` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heritage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `intermediaries_company_email_unique` (`company_email`),
  UNIQUE KEY `intermediaries_email_unique` (`email`),
  UNIQUE KEY `intermediaries_username_unique` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of intermediaries
-- ----------------------------

-- ----------------------------
-- Table structure for intermediary_password_resets
-- ----------------------------
DROP TABLE IF EXISTS `intermediary_password_resets`;
CREATE TABLE `intermediary_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `intermediary_password_resets_email_index` (`email`),
  KEY `intermediary_password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of intermediary_password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for investors
-- ----------------------------
DROP TABLE IF EXISTS `investors`;
CREATE TABLE `investors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cellphone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('0','1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL,
  `capital` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `investors_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of investors
-- ----------------------------

-- ----------------------------
-- Table structure for investor_password_resets
-- ----------------------------
DROP TABLE IF EXISTS `investor_password_resets`;
CREATE TABLE `investor_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `investor_password_resets_email_index` (`email`),
  KEY `investor_password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of investor_password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2017_02_17_213302_create_investors_table', '1');
INSERT INTO `migrations` VALUES ('4', '2017_02_17_213303_create_investor_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('5', '2017_02_17_214242_create_intermediaries_table', '1');
INSERT INTO `migrations` VALUES ('6', '2017_02_17_214243_create_intermediary_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('7', '2017_02_18_213205_create_professionals_table', '1');
INSERT INTO `migrations` VALUES ('8', '2017_02_18_213206_create_professional_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('9', '2017_02_18_213746_create_partners_table', '1');
INSERT INTO `migrations` VALUES ('10', '2017_02_18_213747_create_partner_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('11', '2017_02_20_145717_create_companies_table', '1');
INSERT INTO `migrations` VALUES ('12', '2017_02_20_145718_create_company_password_resets_table', '1');

-- ----------------------------
-- Table structure for partners
-- ----------------------------
DROP TABLE IF EXISTS `partners`;
CREATE TABLE `partners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cellphone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17') COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `geographical` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sector` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `investment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `minimum` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maximum` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avg` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executives` int(11) NOT NULL,
  `funds` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `capitals` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `companies` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exits` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avg_funds` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `partners_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of partners
-- ----------------------------

-- ----------------------------
-- Table structure for partner_password_resets
-- ----------------------------
DROP TABLE IF EXISTS `partner_password_resets`;
CREATE TABLE `partner_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `partner_password_resets_email_index` (`email`),
  KEY `partner_password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of partner_password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for professionals
-- ----------------------------
DROP TABLE IF EXISTS `professionals`;
CREATE TABLE `professionals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cellphone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('0','1','2','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `professionals_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of professionals
-- ----------------------------

-- ----------------------------
-- Table structure for professional_password_resets
-- ----------------------------
DROP TABLE IF EXISTS `professional_password_resets`;
CREATE TABLE `professional_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `professional_password_resets_email_index` (`email`),
  KEY `professional_password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of professional_password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
