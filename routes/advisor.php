<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('advisor')->user();

    //dd($users);

    return view('advisor.home');
})->name('home');

Route::get('/summary/create', 'Company\ExecutiveSummaryController@showSummaryFormForAdvisor');
Route::post('/summary/create', 'Company\ExecutiveSummaryController@createFromAdvisor');
Route::get('/message/create', 'MessageController@showCreateMessageForm');
Route::post('/message/create', 'MessageController@createMessage');
Route::get('/message/{id}', 'MessageController@showMessage');
Route::post('/message', 'MessageController@sendResponse');
Route::get('/message', function() {
  return view('advisor.message');
});
