<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('professional')->user();

    //dd($users);

    return view('professional.home');
})->name('home');

Route::get('/message/create', 'MessageController@showCreateMessageForm');
Route::post('/message/create', 'MessageController@createMessage');
Route::get('/message/{id}', 'MessageController@showMessage_Professional');
Route::post('/message', 'MessageController@sendResponse');
