<?php

Route::get('/home', function () {
    return view('company.home');
})->name('home');

Route::get('/summary', 'Company\ExecutiveSummaryController@showSummaryForm');
Route::post('/summary', 'Company\ExecutiveSummaryController@create');
