<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/login', function () {
    return view('login');
});
Route::post('/login', 'LoginController@login');
Route::get('/summaries', 'Company\ExecutiveSummaryController@showSummariesPage');
Route::get('/summaries/{id}', 'Company\ExecutiveSummaryController@showSummaryPage');
Route::post('/summaries/{id}', 'Company\ExecutiveSummaryController@manageSummary');

Route::group(['prefix' => 'investor'], function () {
  Route::get('/login', 'InvestorAuth\LoginController@showLoginForm');
  Route::post('/login', 'InvestorAuth\LoginController@login');
  Route::post('/logout', 'InvestorAuth\LoginController@logout');

  Route::get('/register', 'InvestorAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'InvestorAuth\RegisterController@register');

  Route::post('/password/email', 'InvestorAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'InvestorAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'InvestorAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'InvestorAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'intermediary'], function () {
  Route::get('/login', 'IntermediaryAuth\LoginController@showLoginForm');
  Route::post('/login', 'IntermediaryAuth\LoginController@login');
  Route::post('/logout', 'IntermediaryAuth\LoginController@logout');

  Route::get('/register', 'IntermediaryAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'IntermediaryAuth\RegisterController@register');

  Route::post('/password/email', 'IntermediaryAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'IntermediaryAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'IntermediaryAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'IntermediaryAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'professional'], function () {
  Route::get('/login', 'ProfessionalAuth\LoginController@showLoginForm');
  Route::post('/login', 'ProfessionalAuth\LoginController@login');
  Route::post('/logout', 'ProfessionalAuth\LoginController@logout');

  Route::get('/register', 'ProfessionalAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'ProfessionalAuth\RegisterController@register');

  Route::post('/password/email', 'ProfessionalAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'ProfessionalAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'ProfessionalAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'ProfessionalAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'partner'], function () {
  Route::get('/login', 'PartnerAuth\LoginController@showLoginForm');
  Route::post('/login', 'PartnerAuth\LoginController@login');
  Route::post('/logout', 'PartnerAuth\LoginController@logout');

  Route::get('/register', 'PartnerAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'PartnerAuth\RegisterController@register');

  Route::post('/password/email', 'PartnerAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'PartnerAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'PartnerAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'PartnerAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'company'], function () {
  Route::get('/login', 'CompanyAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'CompanyAuth\LoginController@login');
  Route::post('/logout', 'CompanyAuth\LoginController@logout');

  Route::get('/register', 'CompanyAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'CompanyAuth\RegisterController@register');

  Route::post('/password/email', 'CompanyAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'CompanyAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'CompanyAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'CompanyAuth\ResetPasswordController@showResetForm');
  Route::get('/register/verify/{token}', 'CompanyAuth\RegisterController@verify');
});

Route::group(['prefix' => 'advisor'], function () {
  Route::get('/login', 'AdvisorAuth\LoginController@showLoginForm');
  Route::post('/login', 'AdvisorAuth\LoginController@login');
  Route::post('/logout', 'AdvisorAuth\LoginController@logout');

  Route::get('/register', 'AdvisorAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'AdvisorAuth\RegisterController@register');

  Route::post('/password/email', 'AdvisorAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'AdvisorAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'AdvisorAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'AdvisorAuth\ResetPasswordController@showResetForm');
});
