<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    public $timestamps = false;

    protected $fillable = [
      'response', 'message', 'date', 'files', 'sender', 'sender_type'
    ];
}
