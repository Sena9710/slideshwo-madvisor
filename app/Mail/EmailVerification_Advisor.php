<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Advisor;

class EmailVerification extends Mailable
{
    use Queueable, SerializesModels;

    public $advisor;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Advisor $advisor)
    {
        $this->advisor = $advisor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.verification_advisor');
    }
}
