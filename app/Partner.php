<?php

namespace App;

use App\Notifications\PartnerResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Partner extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'firstname', 'lastname', 'sex', 'city', 'province', 'zipcode', 'street',
      'dob', 'email', 'cellphone', 'password', 'type', 'website', 'contact',
      'geographical', 'sector', 'investment', 'minimum', 'maximum',
      'executives', 'funds', 'capitals', 'companies', 'exits', 'avg_funds',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PartnerResetPassword($token));
    }
}
