<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapAdvisorRoutes();

        $this->mapCompanyRoutes();

        $this->mapPartnerRoutes();

        $this->mapProfessionalRoutes();

        $this->mapIntermediaryRoutes();

        $this->mapInvestorRoutes();

        //
    }

    /**
     * Define the "investor" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapInvestorRoutes()
    {
        Route::group([
            'middleware' => ['web', 'investor', 'auth:investor'],
            'prefix' => 'investor',
            'as' => 'investor.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/investor.php');
        });
    }

    /**
     * Define the "intermediary" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapIntermediaryRoutes()
    {
        Route::group([
            'middleware' => ['web', 'intermediary', 'auth:intermediary'],
            'prefix' => 'intermediary',
            'as' => 'intermediary.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/intermediary.php');
        });
    }

    /**
     * Define the "professional" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapProfessionalRoutes()
    {
        Route::group([
            'middleware' => ['web', 'professional', 'auth:professional'],
            'prefix' => 'professional',
            'as' => 'professional.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/professional.php');
        });
    }

    /**
     * Define the "partner" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapPartnerRoutes()
    {
        Route::group([
            'middleware' => ['web', 'partner', 'auth:partner'],
            'prefix' => 'partner',
            'as' => 'partner.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/partner.php');
        });
    }

    /**
     * Define the "company" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapCompanyRoutes()
    {
        Route::group([
            'middleware' => ['web', 'company', 'auth:company'],
            'prefix' => 'company',
            'as' => 'company.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/company.php');
        });
    }

    /**
     * Define the "advisor" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapAdvisorRoutes()
    {
        Route::group([
            'middleware' => ['web', 'advisor', 'auth:advisor'],
            'prefix' => 'advisor',
            'as' => 'advisor.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/advisor.php');
        });
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
