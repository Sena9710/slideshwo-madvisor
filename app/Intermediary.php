<?php

namespace App;

use App\Notifications\IntermediaryResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Intermediary extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'type', 'firstname', 'lastname', 'sex', 'company_email', 'province',
      'cellphone', 'email', 'employment_relationship', 'network', 'bank',
      'manager_type', 'experience', 'username', 'company_rank', 'password',
      'heritage'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new IntermediaryResetPassword($token));
    }
}
