<?php

namespace App;

use App\Notifications\CompanyResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Company extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name', 'street', 'zipcode', 'website', 'referrer', 'position', 'email',
      'cellphone', 'fax', 'password', 'description', 'product', 'market',
      'status_development', 'status', 'property', 'managment', 'swot',
      'financial_information', 'previous_investors', 'amount', 'pre_money',
      'specifics', 'why_us', 'verified', 'email_token', 'executive_status',
      'executive_status_reason'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CompanyResetPassword($token));
    }

    public function verified()
    {
        $this->verified = 1;
        $this->email_token = null;
        $this->save();
    }

    public function getFinancialInformation($index)
    {
      $i = 0;
      foreach(explode('$', $this->financial_information) as $table) {
        foreach(explode('|', $table) as $row) {
          foreach(explode('-', $row) as $data) {
            $i++;
            if($data === '') {
              continue;
            }
            if($i == $index) {
              echo $data;
            }
          }
        }
      }
    }

    public function getRequirements($index) {
      $i = 0;
      foreach(explode('|', $this->requirements) as $data) {
        $i++;
        if($data === '') {
          continue;
        }
        if($i == $index) {
          echo $data;
        }
      }
    }
}
