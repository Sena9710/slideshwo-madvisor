<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;
use Validator;
use Illuminate\Support\Facades\Auth;

class ExecutiveSummaryController extends Controller
{
    public function showSummaryForm() {
      return view((Auth::user()->type == '-1') ? 'company.summary' : 'company.summary_compiled');
    }

    public function showSummariesPage() {
      return view('summaries');
    }

    public function showSummaryPage($id) {
      return view('summary', ['company' => Company::where('id', '=', $id)->first()]);
    }

    public function manageSummary(Request $request) {
      $executive = Company::find($request->company_id);
      if($request->action == 'accept') {
        $executive->executive_status = 2;
      } else {
        $executive->executive_status = 3;
        $executive->executive_status_reason = $request->reason;
      }
      $executive->save();

      return redirect('/summaries')->with(
        'message', 'Executive Summary di '.$executive->name.' accettato con successo!'
      );
    }

    public function create(Request $request) {
      $validator = Validator::make($request->all(), [
        'type' => 'required|max:255',
        'description' => 'required|max:255',
        'product' => 'required|max:255',
        'market' => 'required|max:255',
        'status_development' => 'required|max:255',
        'status' => 'required|max:255',
        'property' => 'required|max:255',
        'managment' => 'required|max:255',
        'model_business' => 'required|max:255',
        'previous_investors' => 'required|max:255',
        'amount' => 'required|max:255',
        'pre_money' => 'required|max:255',
        'equity' => 'required|max:255',
        'usage' => 'required|max:255',
        'exit' => 'required|max:255',
        'specifics' => 'required|max:255',
        'why_us' => 'required|max:255'
      ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      $i = 0;
      $files = $request->file('files');
      $property = '';

      if($request->hasFile('files')) {
        foreach ($files as $file) {
          $filename = $file->getClientOriginalName();
          $extension = $file->getClientOriginalExtension();
          $i++;
          $property += (($i == 1) ? ('') : ('|')).$filename.$extesion.'-'.$request->input('value_'.$i);
          $file->store('property/' . Auth::user()->id . '/');
        }
      }

      $file_ = $request->file('business_profile');
      $business_profile = $file_-> $file->getClientOriginalName().$file->getClientOriginalExtension();
      $file_->store('business_profile/'.Auth::user()->id.'/');

      $file_ = $request->file('balance');
      $balance = $file_-> $file->getClientOriginalName().$file->getClientOriginalExtension();
      $file_->store('balance/'.Auth::user()->id.'/');

      $file_ = $request->file('accounting');
      $accounting = $file_-> $file->getClientOriginalName().$file->getClientOriginalExtension();
      $file_->store('accounting/'.Auth::user()->id.'/');

      $company = Company::find($request->company_id);
      $company->type = $request->type;
      $company->description = $request->description;
      $company->product = $request->product;
      $company->market = $request->market;
      $company->status_development = ($request->status_development === 'Altro' ? 'Altro|'.$request->other : $request->status_development );
      $company->status = $request->status;
      $company->property = $request->property;
      $company->managment = $request->managment;
      $company->model_business = $request->model_business;
      $company->swot = $request->punti_di_forza.'|'.$request->punti_di_debolezza.'|'.$request->opportunita.'|'.$request->rischi.'|'.$request->innovazione;
      $company->financial_information =
        $request->prec_fatturato_2012.'-'.$request->prec_fatturato_2013.'-'.$request->prec_fatturato_2014.'-'.$request->prec_fatturato_2015.'-'.$request->prec_fatturato_2015_prev.'|'.
        $request->prec_costi_2012.'-'.$request->prec_costi_2013.'-'.$request->prec_costi_2014.'-'.$request->prec_costi_2015.'-'.$request->prec_costi_2015_prev.'|'.
        $request->prec_netprofit_2012.'-'.$request->prec_netprofit_2013.'-'.$request->prec_netprofit_2014.'-'.$request->prec_netprofit_2015.'-'.$request->prec_netprofit_2015_prev.'|'.
        $request->prec_clienti_2012.'-'.$request->prec_clienti_2013.'-'.$request->prec_clienti_2014.'-'.$request->prec_clienti_2015.'-'.$request->prec_clienti_2015_prev.'$'.
        $request->prec_fatturato_2016.'-'.$request->prec_fatturato_2017.'-'.$request->prec_fatturato_2018.'-'.$request->prec_fatturato_2019.'-'.$request->prec_fatturato_2020_prev.'|'.
        $request->prec_costi_2016.'-'.$request->prec_costi_2017.'-'.$request->prec_costi_2018.'-'.$request->prec_costi_2019.'-'.$request->prec_costi_2020_prev.'|'.
        $request->prec_netprofit_2016.'-'.$request->prec_netprofit_2017.'-'.$request->prec_netprofit_2018.'-'.$request->prec_netprofit_2019.'-'.$request->prec_netprofit_2020_prev.'|'.
        $request->prec_clienti_2016.'-'.$request->prec_clienti_2017.'-'.$request->prec_clienti_2018.'-'.$request->prec_clienti_2019.'-'.$request->prec_clienti_2020_prev
      ;
      $company->previous_investors = $request->previous_investors;
      $company->amount = $request->amount;
      $company->pre_money = $request->pre_money;
      $company->requirements = $request->equity.'|'.$request->usage.'|'.$request->exit;
      $company->specifics = $request->specifics;
      $company->why_us = $request->why_us;
      $company->property_manage = $property;
      $company->business_profile = $business_profile;
      $company->balance = $balance;
      $company->accounting = $accounting;
      $company->save();

      return redirect('home')->with('success', 'Executive Summary inviato con successo!<br>Il nostro staff lo sta visionando e ti risponderà presto');
    }

    public function showSummaryFormForAdvisor() {
      return view('advisor.create_summary');
    }

    public function createFromAdvisor(Request $request) {
      $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
        'street' => 'required|max:255',
        'zipcode' => 'required|max:255',
        'website' => 'required|max:255',
        'referrer' => 'required|max:255',
        'position' => 'required|max:255',
        'email' => 'required|email|max:255|unique:companies',
        'cellphone' => 'required|max:255',
        'fax' => 'max:255',
        'password' => 'required|min:6|confirmed',
        'type' => 'required|max:255',
        'description' => 'required|max:255',
        'product' => 'required|max:255',
        'market' => 'required|max:255',
        'status_development' => 'required|max:255',
        'status' => 'required|max:255',
        'property' => 'required|max:255',
        'managment' => 'required|max:255',
        'model_business' => 'required|max:255',
        'previous_investors' => 'required|max:255',
        'amount' => 'required|max:255',
        'pre_money' => 'required|max:255',
        'equity' => 'required|max:255',
        'usage' => 'required|max:255',
        'exit' => 'required|max:255',
        'specifics' => 'required|max:255',
        'why_us' => 'required|max:255'
      ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      $i = 0;
      $files = $request->file('files');
      $property = '';

      if($request->hasFile('files')) {
        foreach ($files as $file) {
          $filename = $file->getClientOriginalName();
          $extension = $file->getClientOriginalExtension();
          $i++;
          $property += (($i == 1) ? ('') : ('|')).$filename.$extesion.'-'.$request->input('value_'.$i);
          $file->store('property/' . (Company::count() + 1) . '/');
        }
      }

      $file_ = $request->file('business_profile');
      $business_profile = $file_-> $file->getClientOriginalName().$file->getClientOriginalExtension();
      $file_->store('business_profile/'.(Company::count() + 1).'/');

      $file_ = $request->file('balance');
      $balance = $file_-> $file->getClientOriginalName().$file->getClientOriginalExtension();
      $file_->store('balance/'.(Company::count() + 1).'/');

      $file_ = $request->file('accounting');
      $accounting = $file_-> $file->getClientOriginalName().$file->getClientOriginalExtension();
      $file_->store('accounting/'.(Company::count() + 1).'/');

      Company::create([
        'name' => $request->name,
        'street' => $request->street,
        'zipcode' => $request->zipcode,
        'website' => $request->website,
        'referrer' => $request->referrer,
        'position' => $request->position,
        'email' => $request->email,
        'cellphone' => $request->cellphone,
        'fax' => $request->fax,
        'password' => bcrypt($request->password),
        'email_token' => '',
        'type' => $request->type,
        'description' => $request->description,
        'product' => $request->product,
        'market' => $request->market,
        'status_development' => ($request->status_development === 'Altro' ? 'Altro|'.$request->other : $request->status_development ),
        'status' => $request->status,
        'property' => $request->property,
        'managment' => $request->managment,
        'model_business' => $request->model_business,
        'swot' => $request->punti_di_forza.'|'.$request->punti_di_debolezza.'|'.$request->opportunita.'|'.$request->rischi.'|'.$request->innovazione,
        'financial_information' =>
          $request->prec_fatturato_2012.'-'.$request->prec_fatturato_2013.'-'.$request->prec_fatturato_2014.'-'.$request->prec_fatturato_2015.'-'.$request->prec_fatturato_2015_prev.'|'.
          $request->prec_costi_2012.'-'.$request->prec_costi_2013.'-'.$request->prec_costi_2014.'-'.$request->prec_costi_2015.'-'.$request->prec_costi_2015_prev.'|'.
          $request->prec_netprofit_2012.'-'.$request->prec_netprofit_2013.'-'.$request->prec_netprofit_2014.'-'.$request->prec_netprofit_2015.'-'.$request->prec_netprofit_2015_prev.'|'.
          $request->prec_clienti_2012.'-'.$request->prec_clienti_2013.'-'.$request->prec_clienti_2014.'-'.$request->prec_clienti_2015.'-'.$request->prec_clienti_2015_prev.'$'.
          $request->prec_fatturato_2016.'-'.$request->prec_fatturato_2017.'-'.$request->prec_fatturato_2018.'-'.$request->prec_fatturato_2019.'-'.$request->prec_fatturato_2020_prev.'|'.
          $request->prec_costi_2016.'-'.$request->prec_costi_2017.'-'.$request->prec_costi_2018.'-'.$request->prec_costi_2019.'-'.$request->prec_costi_2020_prev.'|'.
          $request->prec_netprofit_2016.'-'.$request->prec_netprofit_2017.'-'.$request->prec_netprofit_2018.'-'.$request->prec_netprofit_2019.'-'.$request->prec_netprofit_2020_prev.'|'.
          $request->prec_clienti_2016.'-'.$request->prec_clienti_2017.'-'.$request->prec_clienti_2018.'-'.$request->prec_clienti_2019.'-'.$request->prec_clienti_2020_prev
        ,
        'previous_investors' => $request->previous_investors,
        'amount' => $request->amount,
        'pre_money' => $request->pre_money,
        'requirements' => $request->equity.'|'.$request->usage.'|'.$request->exit,
        'specifics' => $request->specifics,
        'why_us' => $request->why_us,
        'verified' => '1',
        'email_token' => null,
        'property_manage' => $property,
        'business_profile' => $business_profile,
        'balance' => $balance,
        'accounting' => $accounting,
        'vat' => $request->var
      ]);

      $company = Company::find($request->company_id);
      $company->type = $request->type;
      $company->description = $request->description;
      $company->product = $request->product;
      $company->market = $request->market;
      $company->status_development = ($request->status_development === 'Altro' ? 'Altro|'.$request->other : $request->status_development );
      $company->status = $request->status;
      $company->property = $request->property;
      $company->managment = $request->managment;
      $company->model_business = $request->model_business;
      $company->swot = $request->punti_di_forza.'|'.$request->punti_di_debolezza.'|'.$request->opportunita.'|'.$request->rischi.'|'.$request->innovazione;
      $company->financial_information =
        $request->prec_fatturato_2012.'-'.$request->prec_fatturato_2013.'-'.$request->prec_fatturato_2014.'-'.$request->prec_fatturato_2015.'-'.$request->prec_fatturato_2015_prev.'|'.
        $request->prec_costi_2012.'-'.$request->prec_costi_2013.'-'.$request->prec_costi_2014.'-'.$request->prec_costi_2015.'-'.$request->prec_costi_2015_prev.'|'.
        $request->prec_netprofit_2012.'-'.$request->prec_netprofit_2013.'-'.$request->prec_netprofit_2014.'-'.$request->prec_netprofit_2015.'-'.$request->prec_netprofit_2015_prev.'|'.
        $request->prec_clienti_2012.'-'.$request->prec_clienti_2013.'-'.$request->prec_clienti_2014.'-'.$request->prec_clienti_2015.'-'.$request->prec_clienti_2015_prev.'$'.
        $request->prec_fatturato_2016.'-'.$request->prec_fatturato_2017.'-'.$request->prec_fatturato_2018.'-'.$request->prec_fatturato_2019.'-'.$request->prec_fatturato_2020_prev.'|'.
        $request->prec_costi_2016.'-'.$request->prec_costi_2017.'-'.$request->prec_costi_2018.'-'.$request->prec_costi_2019.'-'.$request->prec_costi_2020_prev.'|'.
        $request->prec_netprofit_2016.'-'.$request->prec_netprofit_2017.'-'.$request->prec_netprofit_2018.'-'.$request->prec_netprofit_2019.'-'.$request->prec_netprofit_2020_prev.'|'.
        $request->prec_clienti_2016.'-'.$request->prec_clienti_2017.'-'.$request->prec_clienti_2018.'-'.$request->prec_clienti_2019.'-'.$request->prec_clienti_2020_prev
      ;
      $company->previous_investors = $request->previous_investors;
      $company->amount = $request->amount;
      $company->pre_money = $request->pre_money;
      $company->requirements = $request->equity.'|'.$request->usage.'|'.$request->exit;
      $company->specifics = $request->specifics;
      $company->why_us = $request->why_us;
      $company->save();

      return redirect('home')->with('success', 'Executive Summary inviato con successo!<br>Il nostro staff lo sta visionando e ti risponderà presto');
    }
}
