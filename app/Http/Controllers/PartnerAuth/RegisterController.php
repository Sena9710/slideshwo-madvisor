<?php

namespace App\Http\Controllers\PartnerAuth;

use App\Partner;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/partner/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('partner.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
          'firstname' => 'required|max:255',
          'lastname' => 'required|max:255',
          'sex' => 'required|max:255',
          'city' => 'required|max:255',
          'province' => 'required|max:255',
          'zipcode' => 'required|max:255',
          'street' => 'required|max:255',
          'dob' => 'required|max:255',
          'email'=> 'required|email|max:255|unique:investors',
          'cellphone' => 'max:255',
          'password' => 'required|min:6|confirmed',
          'type' => 'required|max:255',
          'website' => 'required|max:255',
          'contact' => 'required|max:255',
          'geographical' => 'required|max:255',
          'sector' => 'required|max:255',
          'investment' => 'required|max:255',
          'minimum' => 'required|max:255',
          'maximum' => 'required|max:255',
          'executives' => 'required|max:255',
          'funds' => 'required|max:255',
          'capitals' => 'required|max:255',
          'companies' => 'required|max:255',
          'exits' => 'required|max:255',
          'avg_funds' => 'required|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Partner
     */
    protected function create(array $data)
    {
        return Partner::create([
          'firstname' => $data['firstname'],
          'lastname' => $data['lastname'],
          'sex' => $data['sex'],
          'city' => $data['city'],
          'province' => $data['province'],
          'zipcode' => $data['zipcode'],
          'street' => $data['street'],
          'dob' => $data['dob'],
          'email'=> $data['email'],
          'cellphone' => $data['cellphone'],
          'password' => bcrypt($data['password']),
          'type' => $data['type'],
          'website' => $data['website'],
          'contact' => $data['geographical'],
          'sector' => $data['sector'],
          'investment' => $data['investment'],
          'minimum' => $data['minimum'],
          'maximum' => $data['maximum'],
          'executives' => $data['executives'],
          'funds' => $data['capitals'],
          'companies' => $data['companies'],
          'exits' => $data['exits'],
          'avg_funds' => $data['avg_funds']
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('partner.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('partner');
    }
}
