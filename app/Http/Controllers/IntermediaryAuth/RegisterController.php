<?php

namespace App\Http\Controllers\IntermediaryAuth;

use App\Intermediary;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/intermediary/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('intermediary.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:intermediaries',
            'type' => 'required|max:255',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'sex' => 'required|max:255',
            'company_email' => 'required|email|max:255|unique:intermediaries|confirmed',
            'province' => 'required|max:255',
            'cellphone' => 'max:255',
            'employment_relationship' => 'required|max:255',
            'network' => 'required|max:255',
            'bank' => 'required|max:255',
            'manager_type' => 'required|max:255',
            'experience' => 'required|max:255',
            'username' => 'required|max:255|unique:intermediaries',
            'company_rank' => 'required|max:255',
            'heritage' => 'max:255',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Intermediary
     */
    protected function create(array $data)
    {
        return Intermediary::create([
            'type' => $data['type'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'sex' => $data['sex'],
            'company_email' => $data['company_email'],
            'province' => $data['province'],
            'cellphone' => $data['cellphone'],
            'email' => $data['email'],
            'employment_relationship' => $data['employment_relationship'],
            'network' => $data['network'],
            'bank' => $data['bank'],
            'manager_type' => $data['manager_type'],
            'experience' => $data['experience'],
            'username' => $data['username'],
            'company_rank' => $data['company_rank'],
            'heritage' => $data['heritage'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('intermediary.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('intermediary');
    }
}
