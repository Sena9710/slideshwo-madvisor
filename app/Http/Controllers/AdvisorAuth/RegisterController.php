<?php

namespace App\Http\Controllers\AdvisorAuth;

use DB;
use Mail;
use App\Advisor;
use Validator;
use Illuminate\Http\Request;
use App\Mail\EmailVerification;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/advisor/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('advisor.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
          'firstname' => 'required|max:255',
          'lastname' => 'required|max:255',
          'sex' => 'required|max:255',
          'city' => 'required|max:255',
          'province' => 'required|max:255',
          'zipcode' => 'required|max:255',
          'street' => 'required|max:255',
          'dob' => 'required|max:255',
          'email'=> 'required|email|max:255|unique:advisors',
          'cellphone' => 'max:255',
          'password' => 'required|min:6|confirmed',
          'vat' => 'required|max:255',
          'business_name' => 'required|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Advisor
     */
    protected function create(array $data)
    {
        return Advisor::create([
          'firstname' => $data['firstname'],
          'lastname' => $data['lastname'],
          'sex' => $data['sex'],
          'city' => $data['city'],
          'province' => $data['province'],
          'zipcode' => $data['zipcode'],
          'street' => $data['street'],
          'dob' => $data['dob'],
          'email'=> $data['email'],
          'cellphone' => $data['cellphone'],
          'password' => bcrypt($data['password']),
          'vat' => $data['vat'],
          'business_name' => $data['business_name'],
          'email_token' => str_random(10),
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('advisor.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('advisor');
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails())
        {
            $this->throwValidationException($request, $validator);
        }
        DB::beginTransaction();
        try
        {
            $user = $this->create($request->all());
            $email = new EmailVerification_Advisor(new Advisor(['email_token' => $user->email_token]));
            Mail::to($user->email)->send($email);
            DB::commit();
            return back();
        }
        catch(Exception $e)
        {
            DB::rollback();
            return back();
        }
    }
    public function verify($token)
    {
        Advisor::where('email_token',$token)->firstOrFail()->verified();
        return redirect('/advisor/login');
    }
}
