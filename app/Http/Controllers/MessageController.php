<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\Response;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function showCreateMessageForm() {
      return view('advisor.create_message');
    }

    public function createMessage(Request $request) {
      $files = $request->file('files');

      if($request->hasFile('files')) {
        foreach ($files as $file) {
          $file->store('attachments/' . (Message::count() + 1) . '/');
        }
      }

      $receiver = '';
      if($request->to == 'admin') {
        $receiver = 1;
      }
      Message::create([
        'title' => $request->title,
        'sender' => $request->sender,
        'receiver' => $receiver,
        'receiver_type' => $request->to,
        'files' => '',
        'message' => $request->message
      ]);

      return redirect('/advisor/home')->with('message', 'Messaggio inviato con successo!');
    }

    public function showMessage($id) {
      return view('advisor.message', ['message' => Message::where('id', '=', $id)->where('sender', '=', Auth::user()->id)->first() ]);
    }

    public function showMessage_Professional($id) {
      return view('professional.message', ['message' => Message::where('id', '=', $id)->where('sender', '=', Auth::user()->id)->first() ]);
    }

    public function sendResponse(Request $request) {
      $files = $request->file('files');

      if($request->hasFile('files')) {
        foreach ($files as $file) {
          $file->store('attachments/response/' . (Response::count() + 1) . '/');
        }
      }

      Response::create([
        'sender' => $request->sender,
        'sender_type' => $request->sender_type,
        'response' => $request->response,
        'message' => $request->message
      ]);

      return back();
    }
}
