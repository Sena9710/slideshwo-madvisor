<?php

namespace App\Http\Controllers\CompanyAuth;

use DB;
use Mail;
use App\Company;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use App\Mail\EmailVerification;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/company/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('company.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'street' => 'required|max:255',
            'zipcode' => 'required|max:255',
            'website' => 'required|max:255',
            'referrer' => 'required|max:255',
            'position' => 'required|max:255',
            'email' => 'required|email|max:255|unique:companies',
            'cellphone' => 'required|max:255',
            'fax' => 'max:255',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Company
     */
    protected function create(array $data)
    {
        return Company::create([
          'name' => $data['name'],
          'street' => $data['street'],
          'zipcode' => $data['zipcode'],
          'website' => $data['website'],
          'referrer' => $data['referrer'],
          'position' => $data['position'],
          'email' => $data['email'],
          'cellphone' => $data['cellphone'],
          'fax' => $data['fax'],
          'password' => bcrypt($data['password']),
          'email_token' => str_random(10),
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('company.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('company');
    }

    public function register(Request $request)
    {
        // Laravel validation
        $validator = $this->validator($request->all());
        if ($validator->fails())
        {
            $this->throwValidationException($request, $validator);
        }
        // Using database transactions is useful here because stuff happening is actually a transaction
        // I don't know what I said in the last line! Weird!
        DB::beginTransaction();
        try
        {
            $company = $this->create($request->all());
            // After creating the user send an email with the random token generated in the create method above
            $email = new EmailVerification(new Company(['email_token' => $company->email_token]));
            Mail::to($company->email)->send($email);
            DB::commit();
            return redirect('/company/login')->with('message', 'Registrazione avvenuta con successo!<br>Adesso conferma la tua email!');
        }
        catch(Exception $e)
        {
            DB::rollback();
            return back()->withInput();
        }
    }

    public function verify($token)
    {
        // The verified method has been added to the user model and chained here
        // for better readability
        Company::where('email_token', '=', $token)->firstOrFail()->verified();
        return redirect('/company/summary');
    }
}
