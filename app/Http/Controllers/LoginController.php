<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advisor;
use App\Company;
use App\Intermediary;
use App\Investor;
use App\Partner;
use App\Professional;

class LoginController extends Controller
{
    public function login(Request $request) {
      $exist = false;
      $type = '';
      $result = null;
      $i = 0;
      do {
          switch($i) {
            case 0:
              if(($result = Advisor::where('email', '=', $request->email)->first()) != null) {
                $exist = true;
                $type = 'advisor';
              }
              break;
            case 1:
              if(($result = Company::where('email', '=', $request->email)->first()) != null) {
                $exist = true;
                $type = 'company';
              }
              break;
            case 2:
              if(($result = Intermediary::where('email', '=', $request->email)->first()) != null) {
                $exist = true;
                $type = 'intermediary';
              }
              break;
            case 3:
              if(($result = Investor::where('email', '=', $request->email)->first()) != null) {
                $exist = true;
                $type = 'investor';
              }
              break;
            case 4:
              if(($result = Partner::where('email', '=', $request->email)->first()) != null) {
                $exist = true;
                $type = 'partner';
              }
              break;
            case 5:
              if(($result = Professional::where('email', '=', $request->email)->first()) != null) {
                $exist = true;
                $type = 'professional';
              }
              break;
            default:
              break;
          }
      } while(++$i > 5);
      if($exist) {
        session(['type' => $type]);
        return redirect('/'.$type.'/login')->with('email', $request->email)->with('remember', (($request->remember == 'on') ? 'checked' : ''));
      } else {
        return back()->withErrors('message', 'Hai inserito un email o una password errata');
      }
    }
}
