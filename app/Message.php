<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';
    public $timestamps = false;

    protected $fillable = [
      'sender', 'receiver', 'receiver_type', 'title', 'message', 'date', 'files'
    ];
}
