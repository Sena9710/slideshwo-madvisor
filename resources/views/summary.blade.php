@extends('company.layout.auth')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="pull-left">Dashboard</div>
                      <div class="pull-right">
                        @if ($company->executive_status == 0)
                          <span class="label label-warning">In attesa</span>
                        @elseif ($company->executive_status == 1)
                          <span class="label label-danger">Rifiutato</span>
                        @elseif ($company->executive_status == 2)
                          <span class="label label-success">Accettato</span>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>

                <div class="panel-body">
                  <form class="form-horizontal" role="form" method="POST" action="{{ url('/summaries/') }}/{{ $company->id }}">
                      {{ csrf_field() }}
                      <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                          <label class="col-md-4 control-label">Tipo di azienda</label>

                          <div class="col-md-8">
                            <select class="form-control" name="type" id="type">
                              <option value="0" {{ ($company->type == '0') ? 'selected' : 'disabled hidden' }}>Azienda già esistente</option>
                              <option value="1" {{ ($company->type == '1') ? 'selected': 'disabled hidden' }}>Startup</option>
                            </select>

                              @if ($errors->has('type'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('type') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                          <label for="description" class="col-md-4 control-label">Descrizione breve<br><i>(Massimo 2 righe: Settore industriale – Segmento specifico di business)</i></label>

                          <div class="col-md-6">
                              <textarea id="description" class="form-control" name="description" readonly>{{ $company->description }}</textarea>

                              @if ($errors->has('description'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('description') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('product') ? ' has-error' : '' }}">
                          <label for="product" class="col-md-4 control-label">Prodotto/servizio<br><i>(Massimo 25 righe: Prodotto/Servizio offerto – Elementi distintivi – Beneficio per il cliente)</i></label>

                          <div class="col-md-6">
                              <textarea id="product" class="form-control" name="product" readonly>{{ $company->product }}</textarea>

                              @if ($errors->has('product'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('product') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('market') ? ' has-error' : '' }}">
                          <label for="market" class="col-md-4 control-label">Mercato<br><i>(Massimo 20 righe: Clienti target – Grandezza e previsioni di crescita del Mercato – Proprio Mercato primario di riferimento e futuri Mercati – Strategia di marketing e vendita – Definizione dei competitors – Barriere all’ingresso)</i></label>

                          <div class="col-md-6">
                              <textarea id="market" class="form-control" name="market" readonly>{{ $company->market }}</textarea>

                              @if ($errors->has('market'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('market') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('status_development') ? ' has-error' : '' }}">
                          <label for="status_development" class="col-md-4 control-label">Stadio di sviluppo dell'idea imprenditoriale</label>

                          <div class="col-md-6">
                            <select class="form-control" name="status_development" id="status_development0">
                              <option>{{ (strpos($company->status_development, 'Altro') !== false) ? str_limit($company->status_development, 5) : $company->status_development }}</option>
                            </select>

                              <input type="text"  name="other" class="form-control" id="other" value="{{ substr($company->status_development, 6) }}" readonly>

                              @if ($errors->has('status_development'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('status_development') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                          <label for="status" class="col-md-4 control-label">Massimo 10 righe: Stadio di sviluppo del Prodotto/Servizio offerto – Penetrazione del Mercato target – Prossimi passi</label>

                          <div class="col-md-6">
                              <textarea id="status" class="form-control" name="status" readonly>{{ $company->status }}</textarea>

                              @if ($errors->has('status'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('status') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('property') ? ' has-error' : '' }}">
                          <label for="property" class="col-md-4 control-label">Proprietà intellettuale<br><i>(Massimo 5 righe: Presenza di brevetti e/o copyright – Difendibilità dell’idea di business)</i></label>

                          <div class="col-md-6">
                              <textarea id="property" class="form-control" name="property" readonly>{{ $company->property }}</textarea>

                              @if ($errors->has('property'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('property') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('managment') ? ' has-error' : '' }}">
                          <label for="managment" class="col-md-4 control-label">Managment Team<br><i>(Figure chiave del progetto di impresa e competenze: Nome – Posizione/Ruolo ricoperto – Esperienze pregresse)</i></label>

                          <div class="col-md-6">
                              <textarea id="managment" class="form-control" name="managment" readonly>{{ $company->managment }}</textarea>

                              @if ($errors->has('managment'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('managment') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('model_business') ? ' has-error' : '' }}">
                          <label for="model_business" class="col-md-4 control-label">Modello di business<br><i>(Massimo 20 righe: Modello di vendita – Modello di ricavi – Break Even analysis – Scalabilità del business in contesto internazionale)</i></label>

                          <div class="col-md-6">
                              <textarea id="model_business" class="form-control" name="model_business" readonly>{{ $company->model_business }}</textarea>

                              @if ($errors->has('model_business'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('model_business') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('swot') ? ' has-error' : '' }}">
                          <label for="swot" class="col-md-4 control-label">SWOT ANALYSIS</label>

                          <div class="col-md-6">
                            @foreach(explode('|', $company->swot) as $info)
                              @if ($loop->last)
                                La reale innovativita’ del business consiste <input class="form-control" type="text" value="{{ $info }}" readonly>
                              @else
                                <input type="text" class="form-control" value="{{ $info }}" readonly>
                              @endif
                            @endforeach

                              @if ($errors->has('swot'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('swot') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('financial_information') ? ' has-error' : '' }}">
                          <label for="financial_information" class="col-md-4 control-label">Informazioni finanziarie</label>

                          <div class="col-md-6">
                              <i>FATTURATI ANNI PRECEDENTI</i>
                              <table id="prev_fatturati">
                                <thead>
                                  <tr>
                                    <th>in 000 €/a</th>
                                    <th>2012 reale</th>
                                    <th>2013 reale</th>
                                    <th>2014 reale</th>
                                    <th>2015 reale</th>
                                    <th>2015 prev</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>Fatturato</td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(1) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(2) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(3) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(4) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(5) }}"></td>
                                  </tr>
                                  <tr>
                                    <td>Costi</td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(6) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(7) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(8) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(9) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(10) }}"></td>
                                  </tr>
                                  <tr>
                                    <td>Net Profit</td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(11) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(12) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(13) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(14) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(15) }}"></td>
                                  </tr>
                                  <tr>
                                    <td>Clienti</td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(16) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(17) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(18) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(19) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(20) }}"></td>
                                  </tr>
                                </tbody>
                              </table>
                              <br><i>FATTURATI PREVISIONALI</i>
                              <table>
                                <thead>
                                  <tr>
                                    <th>in 000 €/a</th>
                                    <th>2016 prev</th>
                                    <th>2017 prev</th>
                                    <th>2018 prev</th>
                                    <th>2019 prev</th>
                                    <th>2020 prev</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>Fatturato</td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(21) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(22) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(23) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(24) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(25) }}"></td>
                                  </tr>
                                  <tr>
                                    <td>Costi</td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(26) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(27) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(28) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(29) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(30) }}"></td>
                                  </tr>
                                  <tr>
                                    <td>Net Profit</td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(31) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(32) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(33) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(34) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(35) }}"></td>
                                  </tr>
                                  <tr>
                                    <td>Clienti</td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(36) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(37) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(38) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(39) }}"></td>
                                    <td><input type="text" readonly class="form-control" value="{{ $company->getFinancialInformation(40) }}"></td>
                                  </tr>
                                </tbody>
                              </table>

                              @if ($errors->has('financial_information'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('financial_information') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('previous_investors') ? ' has-error' : '' }}">
                          <label class="col-md-4 control-label">Precedenti Investitori</label>

                          <div class="col-md-8">
                              <div class="radio">
                                <label>
                                  <input id="previous_investors0" type="radio" name="previous_investors" value="0" checked>
                                  {{ ($company->previous_investors == 0) ? 'Si' : 'No' }}
                                </label>
                              </div>

                              @if ($errors->has('previous_investors'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('previous_investors') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                          <label for="amount" class="col-md-4 control-label">Totale ammontare investito </label>

                          <div class="col-md-6">
                              <input id="amount" type="text" readonly class="form-control" name="amount" value="{{ $company->amount }}" >

                              @if ($errors->has('amount'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('amount') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('pre_money') ? ' has-error' : '' }}">
                          <label for="pre_money" class="col-md-4 control-label">Pre-Money Valutation</label>

                          <div class="col-md-6">
                              <input id="pre_money" type="text" readonly class="form-control" name="pre_money" value="{{ $company->pre_money }}" >

                              @if ($errors->has('pre_money'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('pre_money') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('equity') ? ' has-error' : '' }}">
                          <label for="equity" class="col-md-4 control-label">Investimento in equity ricercato  (€): 0.000 da </label>

                          <div class="col-md-6">
                              <input id="equity" type="text" readonly class="form-control" name="equity" value="{{ $company->getRequirements(1) }}" >

                              @if ($errors->has('equity'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('equity') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('usage') ? ' has-error' : '' }}">
                          <label for="usage" class="col-md-4 control-label">Come si intende utilizzare l’investimento </label>

                          <div class="col-md-6">
                              <input id="usage" type="text" readonly class="form-control" name="usage" value="{{ $company->getRequirements(2) }}" >

                              @if ($errors->has('usage'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('usage') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('exit') ? ' has-error' : '' }}">
                          <label for="exit" class="col-md-4 control-label">Exit Strategy prevista</label>

                          <div class="col-md-6">
                              <input id="exit" type="text" readonly class="form-control" name="exit" value="{{ $company->getRequirements(3) }}" >

                              @if ($errors->has('exit'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('exit') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('specifics') ? ' has-error' : '' }}">
                          <label for="specifics" class="col-md-4 control-label">Caratteristiche dell’investitore</label>

                          <div class="col-md-6">
                              Vista la particolarita’ del business e delle nuove feature che dovranno essere sviluppate, si richiede che la figura dell’investitore in questa fase possa apportare <input id="specifics" type="text" readonly class="form-control" name="specifics" value="{{ $company->specifics }}" >

                              @if ($errors->has('specifics'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('specifics') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('property_manage') ? ' has-error' : '' }}">
                          <label for="property_manage" class="col-md-4 control-label">Gestione immobili</label>

                          <div class="col-md-6">
                              <input type="number" id="property_manage" min="0" name="property_manage" value="{{ old('property_manage')  }}">
                              <ul>
                              @if (file_exists(storage_path().'/app/property/'.$company->id.'/'))
                                @forelse(scandir(storage_path().'/app/property/'.$company->id.'/') as $file)
                                @continue($file == '.')
                                @continue($file == '..')
                                <a href="{{ url('/property/'.$company->id.'/') }}/{{ $file }}">{{ str_limit($file, 10) }}</a>
                                @empty
                                <li>Non sono presenti immobili.</li>
                                @endforeach
                              @endif
                              </ul>

                              @if ($errors->has('property_manage'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('property_manage') }}</strong>
                                  </span>
                              @endif
                          </div>

                          <div class="form-group{{ $errors->has('business_profile') ? ' has-error' : '' }}">
                              <label for="business_profile" class="col-md-4 control-label">Visura camerale</label>

                              <div class="col-md-6">
                                <a href="{{ url('/business_profile/'.$company->id.'/') }}/{{ $file }}">{{ str_limit($file, 10) }}</a>

                                  @if ($errors->has('business_profile'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('business_profile') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('business_profile') ? ' has-error' : '' }}">
                          <label for="business_profile" class="col-md-4 control-label">Ultimo bilancio depositato</label>

                          <div class="col-md-6">
                            <a href="{{ url('/balance/'.$company->id.'/') }}/{{ $file }}">{{ str_limit($file, 10) }}</a>

                              @if ($errors->has('business_profile'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('business_profile') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('accounting') ? ' has-error' : '' }}">
                          <label for="accounting" class="col-md-4 control-label">Ultimo bilancio depositato</label>

                          <div class="col-md-6">
                            <a href="{{ url('/accounting/'.$company->id.'/') }}/{{ $file }}">{{ str_limit($file, 10) }}</a>

                              @if ($errors->has('accounting'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('accounting') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>
                  </div>

                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              <button type="submit" class="btn btn-success">
                                  <input type="hidden" name="company_id" value="{{ $company->id }}">
                                  <input type="hidden" name="action" value="accept">
                                  Accetta
                              </button><br>
                              <label>Motivazione: </label> <input type="text" name="reason">
                              <button type="submit" class="btn btn-danger">
                                  <input type="hidden" name="company_id" value="{{ $company->id }}">
                                  <input type="hidden" name="action" value="delete">
                                  Rifiuta
                              </button>
                          </div>
                      </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script
			  src="https://code.jquery.com/jquery-3.1.1.min.js"
			  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			  crossorigin="anonymous"></script>
<script>
$(document).ready(function(){
  @if (strpos($company->status_development, 'Altro') !== false)
    $("#other").show();
  @else
    $("#other").hide();
  @endif
    $('#status_development1').hide();
    $('#type').change(function(){
        var val = $(this).val();
        if(val === "1") {
          $('#status_development1').show();
          $('#status_development0').hide();
          $('#prev_fatturati').hide();
        } else {
          $('#status_development0').show();
          $('#status_development1').hide();
          $('#prev_fatturati').show();
        }
    });
    $("#status_development0").change(function() {
      var val = $(this).val();
      if(val === "9") {
          $("#other").show();
      } else {
          $("#other").hide();
      }
    });
    $("#status_development1").change(function() {
      var val = $(this).val();
      if(val === "9") {
          $("#other").show();
      } else {
          $("#other").hide();
      }
    });
});
</script>
@endsection
