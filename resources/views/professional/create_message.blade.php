@extends('layout.auth_logged')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Messaggi</div>

                <div class="panel-body">
                    <form method="post" class="form-horizontal" action="{{ url('/advisor/message/create') }}" enctype='multipart/form-data'>
                      {{ csrf_field() }}
                      <input type="hidden" name="sender" value="{{ Auth::user()->id }}">

                      <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                          <label for="title" class="col-md-4 control-label">Titolo</label>

                          <div class="col-md-6">
                              <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" autofocus>

                              @if ($errors->has('title'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('title') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">
                          <label for="to" class="col-md-4 control-label">Per</label>

                          <div class="col-md-6">
                              <select id="to" name="to">
                                <option value="admin">Antonio Censabella</option>
                              </select>

                              @if ($errors->has('to'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('to') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                          <label for="message" class="col-md-4 control-label">Messaggio</label>

                          <div class="col-md-6">
                              <textarea id="message" class="form-control" name="message">{{ old('message') }}</textarea>

                              @if ($errors->has('message'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('message') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('attachments') ? ' has-error' : '' }}">
                          <label for="attachments" class="col-md-4 control-label">Allegati</label>

                          <div class="col-md-6">
                              Numero allegati: <input type="number" id="attachments" min="0" name="attachments" value="{{ old('attachments')  }}">

                              <div id="files">

                              </div>

                              @if ($errors->has('attachments'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('attachments') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12">
                          <button type="submit" class="btn btn-success">Invia</button>
                        </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script
			  src="https://code.jquery.com/jquery-3.1.1.min.js"
			  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			  crossorigin="anonymous"></script>
<script>
$(document).ready(function(){
  var value = 0;
  $("[type='number']").keypress(function (evt) {
    evt.preventDefault();
  });
  $('#attachments').on("change paste keyup", function() {
    if(value < 0) return;
    if($('#attachments').val() > value) {
      document.getElementById('files').innerHTML += '<input type="file" name="files[]">';
    } else {
      document.getElementById('files').innerHTML = document.getElementById('files').innerHTML.substring(0, document.getElementById('files').innerHTML.length - 32);
    }
    value = $('#attachments').val();
  });
});
</script>
@endsection
