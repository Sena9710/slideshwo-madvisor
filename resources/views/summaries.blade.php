@extends('company.layout.auth')

@section('content')
<div class="container">
    @if (session('message'))
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="alert alert-dismissible alert-success">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          {{ session('message') }}
        </div>
      </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    <table>
                      <thead>
                        <tr>
                          <th class="col-md-1">#</th>
                          <th class="col-md-9">Azienda/Advisor</th>
                          <th class="col-md-3">Azioni</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach (App\Company::where('executive_status', '=', '0')->cursor() as $executive)
                        <tr>
                          <td>{{ $executive->id }}</td>
                          <td>
                            @if ($executive->advisor == '')
                              {{ $executive->name }}
                            @else
                              {{ App\Advisor::where('id', '=', $executive->advisor)->first()->firstname }} {{ App\Advisor::where('id', '=', $executive->advisor)->first()->lastname }}
                            @endif
                          </td>
                          <td><a href="{{ url('/summaries/') }}/{{ $executive->id }}" class="btn btn-sm btn-primary">Visualizza</a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
