@extends('intermediary.layout.auth')

@section('content')
<div class="container">
            <div class="col-md-10 col-md-offset-1">
                <div style="text-align: center;"><span class="inset span"> REGISTRAZIONE </span></div>
                <div class="panel panel-default">
                    
                    <div class="panel-body">
                      <div class="row">
                      {{ var_dump($errors) }}
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/intermediary/register') }}">
                            {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                                <label for="firstname" class="col-md-4 control-label">Nome</label>

                                <div class="col-md-6">
                                    <input id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" autofocus>

                                    @if ($errors->has('firstname'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('firstname') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                <label for="lastname" class="col-md-4 control-label">Cognome</label>

                                <div class="col-md-6">
                                    <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" autofocus>

                                    @if ($errors->has('lastname'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('lastname') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('sex') ? ' has-error' : '' }}">
                                <label for="sex" class="col-md-4 control-label">Sesso</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="sex" id="sex">
                                      <option value="0" {{ (old('sex') == 0) ? ('selected') : ('') }}>Uomo</option>
                                      <option value="1" {{ (old('sex') == 1) ? ('selected') : ('') }}>Donna</option>
                                    </select>

                                    @if ($errors->has('sex'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('sex') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('company_email') ? ' has-error' : '' }}">
                                <label for="company_email" class="col-md-4 control-label">Email Aziendale</label>

                                <div class="col-md-6">
                                    <input id="company_email" type="email" class="form-control" name="company_email" value="{{ old('company_email') }}">

                                    @if ($errors->has('company_email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('company_email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('company_email_confirmation') ? ' has-error' : '' }}">
                                <label for="company_email_confirmation" class="col-md-4 control-label">Email Aziendale (Conferma)</label>

                                <div class="col-md-6">
                                    <input id="company_email_confirmation" type="email" class="form-control" name="company_email_confirmation" value="{{ old('company_email_confirmation') }}">

                                    @if ($errors->has('company_email_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('company_email_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                <label for="type" class="col-md-4 control-label">Tipo di intermediario</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="type" id="type">
                                      <option value="0" {{ (old('type') == 0) ? ('selected') : ('') }}>Consulenti finanziari</option>
                                      <option value="1" {{ (old('type') == 1) ? ('selected') : ('') }}>Private Banker</option>
                                      <option value="2" {{ (old('type') == 2) ? ('selected') : ('') }}>Asset Manager</option>
                                      <option value="3" {{ (old('type') == 3) ? ('selected') : ('') }}>Promotori</option>
                                      <option value="4" {{ (old('type') == 4) ? ('selected') : ('') }}>Placement Advisor</option>
                                      <option value="5" {{ (old('type') == 5) ? ('selected') : ('') }}>SIM</option>
                                      <option value="6" {{ (old('type') == 6) ? ('selected') : ('') }}>SGR</option>
                                      <option value="7" {{ (old('type') == 7) ? ('selected') : ('') }}>Reti</option>
                                      <option value="8" {{ (old('type') == 8) ? ('selected') : ('') }}>Consulenti vari</option>
                                    </select>

                                    @if ($errors->has('type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('province') ? ' has-error' : '' }}">
                                <label for="province" class="col-md-4 control-label">Provincia</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="province" id="province">
                                      <option value="Agrigento">Agrigento</option>
                                      <option value="Alessandria">Alessandria</option>
                                      <option value="Ancona">Ancona</option>
                                      <option value="Aosta">Aosta</option>
                                      <option value="Arezzo">Arezzo</option>
                                      <option value="Ascoli Piceno">Ascoli Piceno</option>
                                      <option value="Asti">Asti</option>
                                      <option value="Avellino">Avellino</option>
                                      <option value="Bari">Bari</option>
                                      <option value="Barletta-Andria-Trani">Barletta-Andria-Trani</option>
                                      <option value="Belluno">Belluno</option>
                                      <option value="Benevento">Benevento</option>
                                      <option value="Bergamo">Bergamo</option>
                                      <option value="Biella">Biella</option>
                                      <option value="Bologna">Bologna</option>
                                      <option value="Bolzano">Bolzano</option>
                                      <option value="Brescia">Brescia</option>
                                      <option value="Brindisi">Brindisi</option>
                                      <option value="Cagliari">Cagliari</option>
                                      <option value="Caltanissetta">Caltanissetta</option>
                                      <option value="Campobasso">Campobasso</option>
                                      <option value="Carbonia-Iglesias">Carbonia-Iglesias</option>
                                      <option value="Caserta">Caserta</option>
                                      <option value="Catania">Catania</option>
                                      <option value="Catanzaro">Catanzaro</option>
                                      <option value="Chieti">Chieti</option>
                                      <option value="Como">Como</option>
                                      <option value="Cosenza">Cosenza</option>
                                      <option value="Cremona">Cremona</option>
                                      <option value="Crotone">Crotone</option>
                                      <option value="Cuneo">Cuneo</option>
                                      <option value="Enna">Enna</option>
                                      <option value="Fermo">Fermo</option>
                                      <option value="Ferrara">Ferrara</option>
                                      <option value="Firenze">Firenze</option>
                                      <option value="Foggia">Foggia</option>
                                      <option value="Forlì-Cesena">Forlì-Cesena</option>
                                      <option value="Frosinone">Frosinone</option>
                                      <option value="Genova">Genova</option>
                                      <option value="Gorizia">Gorizia</option>
                                      <option value="Grosseto">Grosseto</option>
                                      <option value="Imperia">Imperia</option>
                                      <option value="Isernia">Isernia</option>
                                      <option value="L'Aquila">L'Aquila</option>
                                      <option value="La Spezia">La Spezia</option>
                                      <option value="Latina">Latina</option>
                                      <option value="Lecce">Lecce</option>
                                      <option value="Lecco">Lecco</option>
                                      <option value="Livorno">Livorno</option>
                                      <option value="Lodi">Lodi</option>
                                      <option value="Lucca">Lucca</option>
                                      <option value="Macerata">Macerata</option>
                                      <option value="Mantova">Mantova</option>
                                      <option value="Massa e Carrara">Massa e Carrara</option>
                                      <option value="Matera">Matera</option>
                                      <option value="Medio Campidano">Medio Campidano</option>
                                      <option value="Messina">Messina</option>
                                      <option value="Milano">Milano</option>
                                      <option value="Modena">Modena</option>
                                      <option value="Monza e Brianza">Monza e Brianza</option>
                                      <option value="Napoli">Napoli</option>
                                      <option value="Novara">Novara</option>
                                      <option value="Nuoro">Nuoro</option>
                                      <option value="Ogliastra">Ogliastra</option>
                                      <option value="Olbia-Tempio">Olbia-Tempio</option>
                                      <option value="Oristano">Oristano</option>
                                      <option value="Padova">Padova</option>
                                      <option value="Palermo">Palermo</option>
                                      <option value="Parma">Parma</option>
                                      <option value="Pavia">Pavia</option>
                                      <option value="Perugia">Perugia</option>
                                      <option value="Pesaro e Urbino">Pesaro e Urbino</option>
                                      <option value="Pescara">Pescara</option>
                                      <option value="Piacenza">Piacenza</option>
                                      <option value="Pisa">Pisa</option>
                                      <option value="Pistoia">Pistoia</option>
                                      <option value="Pordenone">Pordenone</option>
                                      <option value="Potenza">Potenza</option>
                                      <option value="Prato">Prato</option>
                                      <option value="Ragusa">Ragusa</option>
                                      <option value="Ravenna">Ravenna</option>
                                      <option value="Reggio Calabria">Reggio Calabria</option>
                                      <option value="Reggio Emilia">Reggio Emilia</option>
                                      <option value="Rieti">Rieti</option>
                                      <option value="Rimini">Rimini</option>
                                      <option value="Roma">Roma</option>
                                      <option value="Rovigo">Rovigo</option>
                                      <option value="Salerno">Salerno</option>
                                      <option value="Sassari">Sassari</option>
                                      <option value="Savona">Savona</option>
                                      <option value="Siena">Siena</option>
                                      <option value="Siracusa">Siracusa</option>
                                      <option value="Sondrio">Sondrio</option>
                                      <option value="Taranto">Taranto</option>
                                      <option value="Teramo">Teramo</option>
                                      <option value="Terni">Terni</option>
                                      <option value="Torino">Torino</option>
                                      <option value="Trapani">Trapani</option>
                                      <option value="Trento">Trento</option>
                                      <option value="Treviso">Treviso</option>
                                      <option value="Trieste">Trieste</option>
                                      <option value="Udine">Udine</option>
                                      <option value="Varese">Varese</option>
                                      <option value="Venezia">Venezia</option>
                                      <option value="Verbano-Cusio-Ossola">Verbano-Cusio-Ossola</option>
                                      <option value="Vercelli">Vercelli</option>
                                      <option value="Verona">Verona</option>
                                      <option value="Vibo Valentia">Vibo Valentia</option>
                                      <option value="Vicenza">Vicenza</option>
                                      <option value="Viterbo">Viterbo</option>
                                    </select>

                                    @if ($errors->has('province'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('province') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('cellphone') ? ' has-error' : '' }}">
                                <label for="cellphone" class="col-md-4 control-label">Cellulare</label>

                                <div class="col-md-6">
                                    <input id="cellphone" type="text" class="form-control" name="cellphone" value="{{ old('cellphone') }}" autofocus>

                                    @if ($errors->has('cellphone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('cellphone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('employment_relationship') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Rapporto di lavoro</label>

                                <div class="col-md-8">
                                    <div class="radio">
                                      <label>
                                        <input id="employment_relationship0" type="radio" name="employment_relationship" value="0">
                                        Agenzia / Mandato / Libera professione
                                      </label>
                                    </div>
                                    <div class="radio">
                                      <label>
                                        <input id="employment_relationship1" type="radio" name="employment_relationship" value="1">
                                        Dipendente Bancario
                                      </label>
                                    </div>

                                    @if ($errors->has('employment_relationship'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('employment_relationship') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('network') ? ' has-error' : '' }}">
                                <label for="network" class="col-md-4 control-label">Rete di appartenenza</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="network" id="network">
                                      <option value="3">ALLIANZ BANK FINANCIAL ADVISORS SPA</option>
                                      <option value="20">ALTO ADIGE BANCA SPA</option>
                                      <option value="88">ALTRA RETE</option>
                                      <option value="11">APOGEO CONSULTING SIM SPA</option>
                                      <option value="10">AZ INVESTIMENTI SIM SPA</option>
                                      <option value="15">AZIMUT CONSULENZA PER INVESTIMENTI SIM SPA</option>
                                      <option value="21">BANCA CARIME SPA</option>
                                      <option value="22">BANCA EUROMOBILIARE SPA</option>
                                      <option value="4">BANCA FIDEURAM SPA</option>
                                      <option value="5">BANCA GENERALI SPA</option>
                                      <option value="23">BANCA INTERMOBILIARE DI INVESTIMENTI E GESTIONI SPA</option>
                                      <option value="8">BANCA IPIBI FINANCIAL ADVISORY SPA</option>
                                      <option value="1">BANCA MEDIOLANUM SPA</option>
                                      <option value="7">BANCA MONTE DEI PASCHI DI SIENA SPA</option>
                                      <option value="24">BANCA NAZIONALE DEL LAVORO SPA</option>
                                      <option value="18">BANCA NUOVA SPA</option>
                                      <option value="25">BANCA PATRIMONI SELLA & C. SPA</option>
                                      <option value="26">BANCA POPOLARE COMMERCIO E INDUSTRIA SPA</option>
                                      <option value="27">BANCA POPOLARE DI ANCONA SPA</option>
                                      <option value="28">BANCA POPOLARE DI BARI SCPA</option>
                                      <option value="29">BANCA POPOLARE DI BERGAMO SPA</option>
                                      <option value="30">BANCA POPOLARE DI MILANO SCRL</option>
                                      <option value="31">BANCA POPOLARE DI PUGLIA E BASILICATA SCPA</option>
                                      <option value="32">BANCA POPOLARE DI SPOLETO SPA</option>
                                      <option value="33">BANCA POPOLARE DI VICENZA SCPA</option>
                                      <option value="34">BANCA REALE SPA</option>
                                      <option value="35">BANCA REGIONALE EUROPEA SPA</option>
                                      <option value="12">BANCA SAI SPA</option>
                                      <option value="36">BANCO DI BRESCIA SAN PAOLO CAB SPA</option>
                                      <option value="37">BANCO DI SAN GIORGIO SPA</option>
                                      <option value="38">CONSULTINVEST INVESTIMENTI SIM SPA</option>
                                      <option value="39">COPERNICO SIM SPA</option>
                                      <option value="14">CREDITO EMILIANO SPA</option>
                                      <option value="6">FINANZA & FUTURO BANCA SPA</option>
                                      <option value="2">FINECOBANK BANCA FINECO SPA</option>
                                      <option value="40">GENESI SIM SPA</option>
                                      <option value="41">GIAMPAOLO ABBONDIO ASSOCIATI SIM SPA</option>
                                      <option value="99">GUEST</option>
                                      <option value="16">HYPO ALPE ADRIA BANK ITALIA SPA</option>
                                      <option value="42">HYPO TIROL BANK AG</option>
                                      <option value="50">IPB SIM</option>
                                      <option value="43">MILLENNIUM SIM SPA</option>
                                      <option value="9">SANPAOLO INVEST SIM SPA</option>
                                      <option value="90">SCONOSCIUTO</option>
                                      <option value="44">SIMGENIA SIM SPA</option>
                                      <option value="45">SOFIA SGR SPA</option>
                                      <option value="46">SOLIDARIETA' & FINANZA SIM SPA</option>
                                      <option value="98">STAFF</option>
                                      <option value="17">UBI BANCA PRIVATE INVESTMENT SPA</option>
                                      <option value="47">UNICASIM SIM SPA</option>
                                      <option value="49">UNIPOL BANCA SPA</option>
                                      <option value="48">VALORI & FINANZA SIM SPA</option>
                                      <option value="13">VENETO BANCA SCPA</option>
                                      <option value="51">WEBANK</option>
                                      <option value="52">WIDIBA</option>
                                    </select>

                                    @if ($errors->has('network'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('network') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('bank') ? ' has-error' : '' }}">
                                <label for="bank" class="col-md-4 control-label">Banca di appartenenza</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="bank" id="bank">
                                      <option value="ANTONVENETAABNAMROSGRSPA">ANTONVENETA ABN AMRO SGR SPA</option>
                                      <option value="BANCAA.G.C.I.SPA">BANCA A.G.C.I. SPA</option>
                                      <option value="BANCAAGRICOLAMANTOVANASPA">BANCA AGRICOLA MANTOVANA SPA</option>
                                      <option value="BANCAALPIMARITTIME-CREDITOCOOPERATIVOCARRU'SCRL">BANCA ALPI MARITTIME - CREDITO COOPERATIVO CARRU' SCRL</option>
                                      <option value="BANCAANTONVENETASPA">BANCA ANTONVENETA SPA</option>
                                      <option value="BANCAAPULIASPA">BANCA APULIA SPA</option>
                                      <option value="BANCAAREAPRATESESOCIETA'COOPERATIVA">BANCA AREA PRATESE SOCIETA' COOPERATIVA</option>
                                      <option value="BANCACARIGEITALIASPA">BANCA CARIGE ITALIA SPA</option>
                                      <option value="BANCACARIGESPA-CASSADIRISPARMIODIGENOVAEIMPERIA">BANCA CARIGE SPA - CASSA DI RISPARMIO DI GENOVA E IMPERIA</option>
                                      <option value="BANCACARIPESPA">BANCA CARIPE SPA</option>
                                      <option value="BANCADELCENTROVENETOCREDITOCOOPERATIVOSCRL">BANCA DEL CENTROVENETO CREDITO COOPERATIVO SCRL</option>
                                      <option value="BANCADELFERMANO-CREDITOCOOPERATIVO-SC">BANCA DEL FERMANO - CREDITO COOPERATIVO - SC</option>
                                      <option value="BANCADELMEZZOGIORNO-MEDIOCREDITOCENTRALES.P.A.">BANCA DEL MEZZOGIORNO - MEDIOCREDITO CENTRALE S.P.A.</option>
                                      <option value="BANCADELMONTEDILUCCASPA">BANCA DEL MONTE DI LUCCA SPA</option>
                                      <option value="BANCADELPIEMONTESPA">BANCA DEL PIEMONTE SPA</option>
                                      <option value="BANCADELSALENTOCREDITOPOPOLARESALENTINOBANCA121SPA">BANCA DEL SALENTO CREDITO POPOLARE SALENTINO BANCA 121 SPA</option>
                                      <option value="BANCADELVALDARNOCREDITOCOOPERATIVOSCRL">BANCA DEL VALDARNO CREDITO COOPERATIVO SCRL</option>
                                      <option value="BANCADELL'ADRIATICOS.P.A.">BANCA DELL'ADRIATICO S.P.A.</option>
                                      <option value="BANCADELL'UMBRIA1462SPA">BANCA DELL'UMBRIA 1462 SPA</option>
                                      <option value="BANCADELLACAMPANIASPA">BANCA DELLA CAMPANIA SPA</option>
                                      <option value="BANCADELLAPROVINCIADIMACERATASPA">BANCA DELLA PROVINCIA DI MACERATA SPA</option>
                                      <option value="BANCADELLEMARCHESPA">BANCA DELLE MARCHE SPA</option>
                                      <option value="BANCADIANGHIARIESTIACREDITOCOOPERATIVO">BANCA DI ANGHIARI E STIA CREDITO COOPERATIVO</option>
                                      <option value="BANCADIBOLOGNACREDITOCOOPERATIVOSOCIETA'COOPERATIVA">BANCA DI BOLOGNA CREDITO COOPERATIVO SOCIETA' COOPERATIVA</option>
                                      <option value="BANCADICARNIAEGEMONESECREDITOCOOPERATIVOSCRL">BANCA DI CARNIA E GEMONESE CREDITO COOPERATIVO SCRL</option>
                                      <option value="BANCADICIVIDALES.P.A.">BANCA DI CIVIDALE S.P.A.</option>
                                      <option value="BANCADICREDITOCOOPERATIVODIALBALANGHEROEROEDELCANAVESE">BANCA DI CREDITO COOPERATIVO DI ALBA  LANGHE  ROERO E DEL CANAVESE</option>
                                      <option value="BANCADICREDITOCOOPERATIVODIBORGHETTOLODIGIANOSCRL">BANCA DI CREDITO COOPERATIVO DI BORGHETTO LODIGIANO SCRL</option>
                                      <option value="BANCADICREDITOCOOPERATIVODICALCIOEDICOVOSCRL">BANCA DI CREDITO COOPERATIVO DI CALCIO E DI COVO SCRL</option>
                                      <option value="BANCADICREDITOCOOPERATIVODICAMBIANO(CASTELFIORENTINO-">BANCA DI CREDITO COOPERATIVO DI CAMBIANO (CASTELFIORENTINO -</option>
                                      <option value="BANCADICREDITOCOOPERATIVODICARATEBRIANZASOCIETA'COOPERATIVA">BANCA DI CREDITO COOPERATIVO DI CARATE BRIANZA SOCIETA' COOPERATIVA</option>
                                      <option value="BANCADICREDITOCOOPERATIVODICARUGATEEINZAGO">BANCA DI CREDITO COOPERATIVO DI CARUGATE E INZAGO</option>
                                      <option value="BANCADICREDITOCOOPERATIVODIDOVERAEPOSTINOSCRL">BANCA DI CREDITO COOPERATIVO DI DOVERA E POSTINO SCRL</option>
                                      <option value="BANCADICREDITOCOOPERATIVODIFIUMICELLOEDAIELLODELFRIULISCRL">BANCA DI CREDITO COOPERATIVO DI FIUMICELLO ED AIELLO DEL FRIULI SCRL</option>
                                      <option value="BANCADICREDITOCOOPERATIVODIINZAGOSCRL">BANCA DI CREDITO COOPERATIVO DI INZAGO SCRL</option>
                                      <option value="BANCADICREDITOCOOPERATIVODIMARCON-VENEZIASCRL">BANCA DI CREDITO COOPERATIVO DI MARCON - VENEZIA SCRL</option>
                                      <option value="BANCADICREDITOCOOPERATIVODIMONTEREALE">BANCA DI CREDITO COOPERATIVO DI MONTEREALE</option>
                                      <option value="BANCADICREDITOCOOPERATIVODIPONTASSIEVESCRL">BANCA DI CREDITO COOPERATIVO DI PONTASSIEVE SCRL</option>
                                      <option value="BANCADICREDITOCOOPERATIVODISANMARZANODISANGIUSEPPE">BANCA DI CREDITO COOPERATIVO DI SAN MARZANO DI SAN GIUSEPPE</option>
                                      <option value="BANCADICREDITOCOOPERATIVODISANPIETROINVINCIOSCRL">BANCA DI CREDITO COOPERATIVO DI SAN PIETRO IN VINCIO SCRL</option>
                                      <option value="BANCADICREDITOCOOPERATIVODISIGNASCRL">BANCA DI CREDITO COOPERATIVO DI SIGNA SCRL</option>
                                      <option value="BANCADICREDITOCOOPERATIVODITRIUGGIOEDELLAVALLEDELLAMBROSC">BANCA DI CREDITO COOPERATIVO DI TRIUGGIO E DELLA VALLE DEL LAMBRO SC</option>
                                      <option value="BANCADICREDITOCOOPERATIVODIVIGNOLEEDELLAMONTAGNA">BANCA DI CREDITO COOPERATIVO DI VIGNOLE E DELLA MONTAGNA</option>
                                      <option value="BANCADICREDITOCOOPERATIVOSENATOREPIETROGRAMMATICODIPACECO">BANCA DI CREDITO COOPERATIVO SENATORE PIETRO GRAMMATICO DI PACECO</option>
                                      <option value="BANCADICREDITOCOOPERATIVOVALLESERIANA">BANCA DI CREDITO COOPERATIVO VALLE SERIANA</option>
                                      <option value="BANCADIIMOLASPA">BANCA DI IMOLA SPA</option>
                                      <option value="BANCADIPESCIACREDITOCOOPERATIVO">BANCA DI PESCIA CREDITO COOPERATIVO</option>
                                      <option value="BANCADIPIACENZASOCIETA'COOPERATIVAPERAZIONI">BANCA DI PIACENZA SOCIETA' COOPERATIVA PER AZIONI</option>
                                      <option value="BANCADIROMAGNASPA">BANCA DI ROMAGNA SPA</option>
                                      <option value="BANCADITRENTOEBOLZANOSPA">BANCA DI TRENTO E BOLZANO SPA</option>
                                      <option value="BANCADIUDINECREDITOCOOPERATIVO">BANCA DI UDINE CREDITO COOPERATIVO</option>
                                      <option value="BANCAMONTEPARMASPA">BANCA MONTE PARMA SPA</option>
                                      <option value="BANCAPADOVANACREDITOCOOPERATIVOS.C.">BANCA PADOVANA CREDITO COOPERATIVO S.C.</option>
                                      <option value="BANCAPOPOLARECOMMERCIOEINDUSTRIASPA">BANCA POPOLARE COMMERCIO E INDUSTRIA SPA</option>
                                      <option value="BANCAPOPOLAREDELLAZIOSOCIETA'COOPERATIVAPERAZIONI">BANCA POPOLARE DEL LAZIO SOCIETA' COOPERATIVA PER AZIONI</option>
                                      <option value="BANCAPOPOLAREDELMEZZOGIORNOSPA">BANCA POPOLARE DEL MEZZOGIORNO SPA</option>
                                      <option value="BANCAPOPOLAREDELL'ADRIATICOSPA">BANCA POPOLARE DELL'ADRIATICO SPA</option>
                                      <option value="BANCAPOPOLAREDELL'ALTOADIGESOCIETA'COOPERATIVAPERAZIONI">BANCA POPOLARE DELL'ALTO ADIGE SOCIETA' COOPERATIVA PER AZIONI</option>
                                      <option value="BANCAPOPOLAREDELL'EMILIAROMAGNASOCIETA'COOPERATIVA">BANCA POPOLARE DELL'EMILIA ROMAGNA  SOCIETA' COOPERATIVA</option>
                                      <option value="BANCAPOPOLAREDELL'ETRURIAEDELLAZIOSCARL">BANCA POPOLARE DELL'ETRURIA E DEL LAZIO SCARL</option>
                                      <option value="BANCAPOPOLAREDIANCONASPA">BANCA POPOLARE DI ANCONA SPA</option>
                                      <option value="BANCAPOPOLAREDIBARISOCIETA'COOPERATIVAPERAZIONI">BANCA POPOLARE DI BARI SOCIETA' COOPERATIVA PER AZIONI</option>
                                      <option value="BANCAPOPOLAREDIBERGAMOSPA">BANCA POPOLARE DI BERGAMO SPA</option>
                                      <option value="BANCAPOPOLAREDIMAROSTICASOCIETA'COOPERATIVAPERAZIONIAR.L.">BANCA POPOLARE DI MAROSTICA SOCIETA' COOPERATIVA PER AZIONI A R.L.</option>
                                      <option value="BANCAPOPOLAREDIMILANOSCARL">BANCA POPOLARE DI MILANO SCARL</option>
                                      <option value="BANCAPOPOLAREDISONDRIOSOCIETA'COOPERATIVAPERAZIONI">BANCA POPOLARE DI SONDRIO  SOCIETA' COOPERATIVA PER AZIONI</option>
                                      <option value="BANCAPOPOLAREDISPOLETOSPA">BANCA POPOLARE DI SPOLETO SPA</option>
                                      <option value="BANCAPOPOLAREETICASOCIETA'COOPERATIVAPERAZIONI">BANCA POPOLARE ETICA SOCIETA' COOPERATIVA PER AZIONI</option>
                                      <option value="BANCAPOPOLAREFRIULADRIASPA">BANCA POPOLARE FRIULADRIA SPA</option>
                                      <option value="BANCAPOPOLAREPUGLIESESOCIETA'COOPERATIVAPERAZIONI">BANCA POPOLARE PUGLIESE SOCIETA' COOPERATIVA PER AZIONI</option>
                                      <option value="BANCAPROMOSSPA">BANCA PROMOS SPA</option>
                                      <option value="BancaProssimaSpa">Banca Prossima Spa</option>
                                      <option value="BANCAREGIONALEEUROPEASPA">BANCA REGIONALE EUROPEA SPA</option>
                                      <option value="BANCASELLA-SPA">BANCA SELLA - SPA</option>
                                      <option value="BANCASELLAHOLDINGSPA">BANCA SELLA HOLDING SPA</option>
                                      <option value="BANCASIMETICASPA">BANCA SIMETICA SPA</option>
                                      <option value="BANCASISTEMASPA">BANCA SISTEMA SPA</option>
                                      <option value="BANCASUASA-CREDITOCOOPERATIVO-SOCIETA'COOPERATIVAA">BANCA SUASA - CREDITO COOPERATIVO - SOCIETA' COOPERATIVA A</option>
                                      <option value="BANCAVERONESEDICREDITOCOOPERATIVODICONCAMARISE">BANCA VERONESE DI CREDITO COOPERATIVO DI CONCAMARISE</option>
                                      <option value="BANCODIBRESCIASANPAOLOCABSPA">BANCO DI BRESCIA SAN PAOLO CAB SPA</option>
                                      <option value="BANCODICREDITOP.AZZOAGLIO">BANCO DI CREDITO P.AZZOAGLIO</option>
                                      <option value="BANCODINAPOLISPA">BANCO DI NAPOLI SPA</option>
                                      <option value="BANCODISANGIORGIOSPA">BANCO DI SAN GIORGIO SPA</option>
                                      <option value="BANCOPOPOLARESOCIETA'COOPERATIVA">BANCO POPOLARE SOCIETA' COOPERATIVA</option>
                                      <option value="BARCLAYSBANKPLC">BARCLAYS BANK PLC</option>
                                      <option value="CASSADEIRISPARMIDIFORLI'EDELLAROMAGNASPA">CASSA DEI RISPARMI DI FORLI' E DELLA ROMAGNA SPA</option>
                                      <option value="CASSADEIRISPARMIDIMILANOEDELLALOMBARDIASPA">CASSA DEI RISPARMI DI MILANO E DELLA LOMBARDIA SPA</option>
                                      <option value="CASSADIRISPARMIODELFRIULIVENEZIAGIULIASPA">CASSA DI RISPARMIO DEL FRIULI VENEZIA GIULIA SPA</option>
                                      <option value="CASSADIRISPARMIODELVENETOSPA">CASSA DI RISPARMIO DEL VENETO SPA</option>
                                      <option value="CASSADIRISPARMIODELLAPROVINCIADICHIETISPA">CASSA DI RISPARMIO DELLA PROVINCIA DI CHIETI SPA</option>
                                      <option value="CASSADIRISPARMIODELLAPROVINCIADIVITERBOSPA">CASSA DI RISPARMIO DELLA PROVINCIA DI VITERBO SPA</option>
                                      <option value="CASSADIRISPARMIODELLASPEZIASPA">CASSA DI RISPARMIO DELLA SPEZIA SPA</option>
                                      <option value="CASSADIRISPARMIODIASTISPA">CASSA DI RISPARMIO DI ASTI SPA</option>
                                      <option value="CASSADIRISPARMIODIBIELLAEVERCELLI-BIVERBANCASPA">CASSA DI RISPARMIO DI BIELLA E VERCELLI - BIVERBANCA SPA</option>
                                      <option value="CASSADIRISPARMIODIBOLZANOSPA">CASSA DI RISPARMIO DI BOLZANO SPA</option>
                                      <option value="CASSADIRISPARMIODICARRARASPA">CASSA DI RISPARMIO DI CARRARA SPA</option>
                                      <option value="CASSADIRISPARMIODICENTOSPA">CASSA DI RISPARMIO DI CENTO SPA</option>
                                      <option value="CASSADIRISPARMIODICESENASPA">CASSA DI RISPARMIO DI CESENA SPA</option>
                                      <option value="CASSADIRISPARMIODICIVITAVECCHIASPA">CASSA DI RISPARMIO DI CIVITAVECCHIA SPA</option>
                                      <option value="CASSADIRISPARMIODIFABRIANOECUPRAMONTANASPA">CASSA DI RISPARMIO DI FABRIANO E CUPRAMONTANA SPA</option>
                                      <option value="CASSADIRISPARMIODIFERMOSPA">CASSA DI RISPARMIO DI FERMO SPA</option>
                                      <option value="CASSADIRISPARMIODIFERRARASPA">CASSA DI RISPARMIO DI FERRARA SPA</option>
                                      <option value="CASSADIRISPARMIODIFIRENZESPA-BANCACRFIRENZE">CASSA DI RISPARMIO DI FIRENZE SPA - BANCA CR FIRENZE</option>
                                      <option value="CASSADIRISPARMIODIPARMAEPIACENZASPA-CARIPARMA">CASSA DI RISPARMIO DI PARMA E PIACENZA SPA - CARIPARMA</option>
                                      <option value="CASSADIRISPARMIODIPISTOIAEDELLALUCCHESIAS.P.A.">CASSA DI RISPARMIO DI PISTOIA E DELLA LUCCHESIA S.P.A.</option>
                                      <option value="CASSADIRISPARMIODIRAVENNASPA">CASSA DI RISPARMIO DI RAVENNA SPA</option>
                                      <option value="CASSADIRISPARMIODIRIETISPA">CASSA DI RISPARMIO DI RIETI SPA</option>
                                      <option value="CASSADIRISPARMIODISANMINIATOSPA">CASSA DI RISPARMIO DI SAN MINIATO SPA</option>
                                      <option value="CASSADIRISPARMIODISAVONASPA">CASSA DI RISPARMIO DI SAVONA SPA</option>
                                      <option value="CASSADIRISPARMIOINBOLOGNAS.P.A.INSIGLACARISBOSPA">CASSA DI RISPARMIO IN BOLOGNA S.P.A. IN SIGLA CARISBO SPA</option>
                                      <option value="CASSAPADANABCCSOCIETA'COOPERATIVA">CASSA PADANA BCC SOCIETA' COOPERATIVA</option>
                                      <option value="CASSARAIFFEISENCASTELROTTO-ORTISEISOC.COOPERATIVA">CASSA RAIFFEISEN CASTELROTTO - ORTISEI SOC. COOPERATIVA</option>
                                      <option value="CASSARAIFFEISENDIBRUNICO">CASSA RAIFFEISEN DI BRUNICO</option>
                                      <option value="CASSARAIFFEISENDILANA-RAIFFEISENKASSELANA">CASSA RAIFFEISEN DI LANA - RAIFFEISENKASSE LANA</option>
                                      <option value="CASSARAIFFEISENMERANOSOC.COOP.-RAIFFEISENKASSEMERAN">CASSA RAIFFEISEN MERANO SOC. COOP. - RAIFFEISENKASSE MERAN</option>
                                      <option value="CASSARAIFFEISENNOVALEVANTE">CASSA RAIFFEISEN NOVA LEVANTE</option>
                                      <option value="CASSARURALEDELCREMASCOBANCADICREDITOCOOPERATIVOSCRL">CASSA RURALE DEL CREMASCO BANCA DI CREDITO COOPERATIVO SCRL</option>
                                      <option value="CASSARURALEDICENTROFIEMMECAVALESEBANCADICREDITOCOOPERATIVO">CASSA RURALE DI CENTROFIEMME CAVALESE BANCA DI CREDITO COOPERATIVO</option>
                                      <option value="CASSARURALEDIGIOVO">CASSA RURALE DI GIOVO</option>
                                      <option value="CASSARURALEDILAVISVALLEDICEMBRABCC">CASSA RURALE DI LAVIS VALLE DI CEMBRA BCC</option>
                                      <option value="CASSARURALEDIOLLESAMONESCURELLE-BANCADICREDITOCOOPERATIVO">CASSA RURALE DI OLLE SAMONE SCURELLE - BANCA DI CREDITO COOPERATIVO</option>
                                      <option value="CASSARURALEEDARTIGIANADIBINASCO-CREDITOCOOPERATIVO">CASSA RURALE ED ARTIGIANA DI BINASCO - CREDITO COOPERATIVO</option>
                                      <option value="CASSARURALEEDARTIGIANADIRIVAROLOMANTOVANO-CREDITOCOOPERATIVO">CASSA RURALE ED ARTIGIANA DI RIVAROLO MANTOVANO - CREDITO COOPERATIVO</option>
                                      <option value="CASSARURALEVALLIDIPRIMIEROEVANOI-BANCADICREDITOCOOPERATIVO">CASSA RURALE VALLI DI PRIMIERO E VANOI - BANCA DI CREDITO COOPERATIVO</option>
                                      <option value="CASSARURALEVALSUGANAETESINO-BANCADICREDITOCOOPERATIVO-">CASSA RURALE VALSUGANA E TESINO - BANCA DI CREDITO COOPERATIVO -</option>
                                      <option value="CASSEDIRISPARMIODELL'UMBRIAS.P.A.">CASSE DI RISPARMIO DELL'UMBRIA S.P.A.</option>
                                      <option value="CHEBANCA!SPA">CHEBANCA! SPA</option>
                                      <option value="CREDITOCOOPERATIVOUMBRO-BCCMANTIGNANA-SOCIETA'COOPERATIVA">CREDITO COOPERATIVO UMBRO - BCC MANTIGNANA - SOCIETA' COOPERATIVA</option>
                                      <option value="CREDITODIROMAGNASPA">CREDITO DI ROMAGNA SPA</option>
                                      <option value="DEUTSCHEBANKSPA">DEUTSCHE BANK SPA</option>
                                      <option value="FARBANCASPA">FARBANCA SPA</option>
                                      <option value="ICCREABANCAIMPRESAS.P.A.">ICCREA BANCAIMPRESA S.P.A.</option>
                                      <option value="INGDIRECTN.V.">ING DIRECT N.V.</option>
                                      <option value="INTESASANPAOLOSPA">INTESA SANPAOLO SPA</option>
                                      <option value="INVESTBANCASPA">INVEST BANCA SPA</option>
                                      <option value="ISTITUTOCENTRALEDELLEBANCHEPOPOLARIITALIANESPA">ISTITUTO CENTRALE DELLE BANCHE POPOLARI ITALIANE SPA</option>
                                      <option value="NORDESTBANCASPA">NORDEST BANCA SPA</option>
                                      <option value="POSTEITALIANESPA">POSTE ITALIANE SPA</option>
                                      <option value="UNICREDITSPA">UNICREDIT SPA</option>
                                      <option value="UNIPOLBANCASPA">UNIPOL BANCA SPA</option>
                                      <option value="ALTRA">ALTRA</option>
                                    </select>

                                    @if ($errors->has('bank'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('bank') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group{{ $errors->has('manager_type') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Tipo di gestore</label>

                                <div class="col-md-8">
                                    <div class="radio">
                                        <label>
                                      <input id="Mass Market" type="radio" name="manager_type" value="0"> Mass Market
                                        </label>
                                    </div>
                                      <div class="radio">
                                        <label>
                                          <input id="Affluent" type="radio" name="manager_type" value="1"> Affluent
                                        </label>
                                    </div>
                                      <div class="radio">
                                        <label>
                                          <input id="HNWI" type="radio" name="manager_type" value="2"> HNWI
                                        </label>
                                    </div>
                                      <div class="radio">
                                        <label>
                                          <input id="Ultra HNWI" type="radio" name="manager_type" value="3"> Ultra HNWI
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                          <input id="Altro" type="radio" name="manager_type" value="4"> Altro
                                        </label>
                                    </div>

                                    @if ($errors->has('manager_type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('manager_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('experience') ? ' has-error' : '' }}">
                                <label for="experience" class="col-md-4 control-label">Esperienza nel settore</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="experience" id="experience">
                                      <option value="Fino a 1 anno">Fino a 1 anno</option>
                                      <option value="Fino a 5 anni">Fino a 5 anni</option>
                                      <option value="Fino a 15 anni">Fino a 15 anni</option>
                                      <option value="Piu di 15 anni">Pi&ugrave; di 15 anni</option>
                                    </select>

                                    @if ($errors->has('experience'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('experience') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <label for="username" class="col-md-4 control-label">Username</label>

                                <div class="col-md-6">
                                    <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" autofocus>

                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('company_rank') ? ' has-error' : '' }}">
                                <label for="company_rank" class="col-md-4 control-label">Posizione aziendale</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="company_rank" id="company_rank">
                                      <option value="Consulente Finanziario">Consulente Finanziario (Ex Promotore Finanziario)</option>
                                      <option value="Area Manager">Area Manager</option>
                                      <option value="Regional Manager">Regional Manager</option>
                                      <option value="District Manager">District Manager</option>
                                      <option value="Group Manager">Group Manager</option>
                                      <option value="Consulente Finanziario Autonomo">Consulente Finanziario Autonomo (Ex Consulente Finanziario)</option>
                                      <option value="Dirigente">Dirigente/Quadro</option>
                                      <option value="Private Banker">Private Banker</option>
                                      <option value="Consulente/Gestore affluent">Consulente/Gestore Affluent</option>
                                      <option value="Supervisore">Supervisore</option>
                                      <option value="Consulente/Mass Market">Consulente/Mass Market</option>
                                    </select>

                                    @if ($errors->has('company_rank'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('company_rank') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('heritage') ? ' has-error' : '' }}">
                                <label for="heritage" class="col-md-4 control-label">Posizione aziendale</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="heritage" id="heritage">
                                      <option value="Meno di 10.000.000">Meno di 10.000.000</option>
                                      <option value="Da 10.000.000 a 20.000.000">Da 10.000.000 a 20.000.000</option>
                                      <option value="Da 20.000.000 a 50.000.000">Da 20.000.000 a 50.000.000</option>
                                      <option value="Pi&ugrave; di 50.000.000">Pi&ugrave di 50.000.000</option>
                                    </select>

                                    @if ($errors->has('heritage'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('heritage') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        </form>
                      </div>
                        
            <div class="col-md-12" style="text-align: center;">
              <div class="form-group">
                  <div class="row">
                      <div class="col-md-12">
                        <input type="checkbox" id="tos" required> <label for="tos">Confermo di aver letto e di accettare le condizioni generali di servizio</label>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <input type="checkbox" id="privacy" required> <label for="privacy"> Confermo di aver letto e di accettare il testo sulla privacy </label>
                    </div>
                  </div>
              </div>
            
              <div class="form-group">
                  <div class="col-md-12"">
                      <button type="submit" class="btn btn-primary">
                          Register
                      </button>
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script
			  src="https://code.jquery.com/jquery-3.1.1.min.js"
			  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			  crossorigin="anonymous"></script>
<script>
$(document).ready(function(){
  $('#bank').fadeOut('slow');
    $('#employment_relationship0').change(function(){
        if(this.checked) {
            $('#bank').fadeOut('slow');
            $('#network').fadeIn('slow');
        }
        else {
            $('#bank').fadeIn('slow');
            $('#network').fadeOut('slow');
        }
    });
    $('#employment_relationship1').change(function(){
        if(this.checked) {
            $('#bank').fadeIn('slow');
            $('#network').fadeOut('slow');
        }
        else {
            $('#bank').fadeOut('slow');
            $('#network').fadeIn('slow');
        }
    });
});
</script>
