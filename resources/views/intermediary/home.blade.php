@extends('layout.auth_logged')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in as Intermediary!
                </div>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Executive Summary</div>

            <div class="panel-body">
                Attualmente ci sono <strong>{{ App\Company::where('executive_status', '=', '0')->count() }}</strong> Executive Summary in attesa di revisione.
                <br>
                @if (App\Company::where('executive_status', '=', '0')->count() > 0)
                  <a href="{{ url('/summaries') }}" class="btn btn-primary pull-right">Visualizza ora</a>
                @endif
            </div>
        </div>
      </div
    </div>
</div>
@endsection
