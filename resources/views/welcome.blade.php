<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Mediterraneo Advisor</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link href="css/all.css" rel="stylesheet">

    <!-- carousel -->
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css'>

    <link rel="stylesheet" href="css/style.css">
    <!-- END carousel -->
    
</head>
<body>
    <nav class="navbar navbar-toggleable-md navbar-dark fixed-top scrolling-navbar">
      <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav1" aria-controls="navbarNav1" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">
          <img class="widthm" src="images/logo-mediterraneo.png">
        </a>
        <div class="collapse navbar-collapse" id="navbarNav1">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link">Features</a>
            </li>
            <li class="nav-item">
              <a class="nav-link">Pricing</a>
            </li>
            <li class="nav-item btn-group">
              <a class="nav-link dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <a class="dropdown-item nav-link">Link</a>
                <a class="dropdown-item nav-link">Link</a>
                <a class="dropdown-item nav-link">Link</a>
              </div>
            </li>
            <li class="nav-item btn-group">
              <a class="nav-link dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                <a class="dropdown-item nav-link">Link</a>
                <a class="dropdown-item nav-link">Link</a>
                <a class="dropdown-item nav-link">Link</a>
              </div>
            </li>
            <li class="nav-item btn-group">
              <a class="nav-link dropdown-toggle" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenu3">
                <a class="dropdown-item nav-link">Link</a>
                <a class="dropdown-item nav-link">Link</a>
                <a class="dropdown-item nav-link">Link</a>
              </div>
            </li>
          </ul>
          <ul class="navbar-right">
            <li class="nav-item btn-group">
              <a class="nav-link btn btn-default btn-lg"><i class="fa fa-user fa-2x"></i> Accedi</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="view hm-black-strong">
      <div class="full-bg-img flex-center">
        <ul>
          <li>
            <h1 class="h1-responsive wow fadeInDown" data-wow-delay="0.2s">MEDITERRANEO ADVISOR</h1>
          </li>
          <li>
            <p class="wow fadeInDown">Advisor fondi di investimento per le aziende del Mediterraneo</p>
          </li>
          <li>
            <a target="_blank" href="#" class="btn btn-primary btn-lg wow fadeInLeft" data-wow-delay="0.2s">Registrati ora!</a>
            <a target="_blank" href="#" class="btn btn-default btn-lg wow fadeInRight" data-wow-delay="0.2s">Leggi di piu</a>
          </li>
        </ul>
      </div>
    </div>

    <section id="section1">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="divider-new text-center">
              <h2 class="h2-responsive wow fadeInDown">About us</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
              <p>Abbiamo il piacere e l'onore di rappresentare in tutta l'Unione Europea moltissimi fondi di pre-seed capital, fondi di seed capital, fondi di venture capital, fondi di private equity, fondi di turnaround, fondi di replacement, fondi di passaggio generazionale,fondi per l'internazionalizzazione, fondi per sostituzione soci, fondi per spin-off, fondi per sviluppo commerciale, fondi per vuoto manageriale, fondi per start-up, fondi per emissione mini-bond, fondi per quotazione in Borsa. Non solo italiani.
              Nella qualità di Investor Relations Company seguiamo con grandissima passione le storie d'oltreoceano di ragazzi in jeans e maglietta che si trovano catapultati in un batter d'occhio all'attenzione dei mass media mondiali, per il "solo" fatto di aver creato, molto spesso dal nulla, un software, un sito internet, un servizio, un impresa esplosiva; una app. Tutto questo e' successo anche a noi.....
              Le aree di intervento in cui operiamo, sono quelle relative ai Fondi di Venture Capital, Private Equity, Replacement, Spin-off, Ristrutturazione di Aziende in Crisi, Operazioni di Sostituzione Soci, Fusioni e Acquisizioni, Passaggio Generazionale. </p>
              <p><strong>Mediterraneo Advisor</strong> e' fondatore e advisor esclusivo di MEDITERRANEO CAPITAL Ltd, Holding di investimenti nel capitale di rischio prevalentemente dedicato alle aziende del Mediterraneo.</p>
          </div>
          <div class="col-md-6">
              <p><strong>Mediterraneo Advisor</strong> ha a cuore lo sviluppo economico della Sicilia, della Calabria, della Campania, della Basilicata, del Molise, dell'Abruzzo, del Lazio, della Puglia, delle Marche, dell'Umbria, della Sardegna. ALBANIA, ALGERIA, BOSNIA, CIPRO, CROAZIA, EGITTO, FRANCIA ,GRECIA, ISRAELE, LIBANO, LIBIA, MALTA, MAROCCO, MONTENEGRO, SPAGNA, PALESTINA, SIRIA, TUNISIA ,TURCHIA, GIBILTERRA, MONACO, SLOVENIA.</p>
              <p>La nostra attivita' di scouting e' concentrata nei seguenti settori specifici di investimento :
              Agro-Alimentare, Aerospazio, Automazione Industriale, Automotive, Beni di Consumo, Beni di Lusso, Biotech, Informatica, Elettronica, Energia, Food & Beverage, ICT, Infrastrutture, Manifatturiero, Prodotti e Servizi per l'Industria, Retail, Robotica, Telecomunicazioni, Trasporti e Logistica. Abbigliamento, Aerospazio, Alimentare, Ambiente, Architettura, Arredamento, Arte, Beni di Consumo, Brokeraggio , Carta e stampa, Chimica e plastica, Commercio internazionale, Consulenza aziendale, Costruzioni, Credito e finanza, Edilizia e lavori pubblici , Editoria, Media, Intrattenimento , Elettrotecnica, Formazione, Forniture per ufficio, ICT, Marketing e comunicazione, Meccanica, Medicina ed estetica, Nautica e Shipping, Preziosi e Accessori , Robotica, Salute e benessere , Servizi alle imprese, Servizi finanziari , Sport e tempo libero, Studi legali , Viaggi e turismo. </p>
          </div>
        </div>
      </div>
    </div>

    <section id="second">
      <br>
      <div class="container">
        <div class="row">
          <div class="col-md-3 text-center">
            <div class="row icon">
              <img src="images/tech.png">
            </div>
            <div class="row">
              <p>Siamo tra i primi operatori di venture al mondo ad aver apportare la tecnologia nella selezione e gestione delle operazioni di investimento.</p>
            </div>
          </div>
          <div class="col-md-3 text-center">
            <div class="row icon">
              <img src="images/money.png">
            </div>
            <div class="row">
              <p>Leader nel centro-sud Italia nelle operazione di capitali di rischio</p>
            </div>
          </div>
          <div class="col-md-3 text-center">
            <div class="row icon">
              <img src="images/house.png">
            </div>
            <div class="row">
              <p>Tra i pochi operatori internazionali ad effettuare sia operazioni di venture che operazioni immobiliari</p>
            </div>
          </div>
          <div class="col-md-3 text-center">
            <div class="row icon">
              <img src="images/world.png">
            </div>
            <div class="row">
              <p>Lavoriamo con oltre 500 investitori in tutto il mondo</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="second2">
      <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="divider-new text-center">
                <h2 class="h2-responsive wow fadeInDown">Best features</h2>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="item-campaign col-md-6">
              <div class="item-campaign-wrapper">
                <div class="entry-thumbnail text-center">
                  <img src="images/angel2.jpg" class="attachment-thumbnails-crowdfunding wp-post-image" alt="campaign-8">
                </div>
                <div class="body-inner">
                  <div class="entry-heading">
                    <h3 class="entry-title text-center"><span>Investor relations management</span></h3>
                  </div>
                  <div class="campaign-main-style-3">
                    L'Investor Relator Manager è il soggetto incaricato da una Società quotata o non quotata, della gestione dei rapporti con gli investitori professionali, informali ed istituzionali.
                    <br>Siamo l'unico Investor Relator Company professionale dell'intero centro-sud Italia, Lazio, Abruzzo, Toscana ed Emilia Romagna compresi.
                  </div>
                </div>
              </div>
            </div>
            <div class="item-campaign col-md-6">
              <div class="item-campaign-wrapper">
                <div class="entry-thumbnail text-center">
                  <img src="images/FUSIONI3.jpg" class="attachment-thumbnails-crowdfunding wp-post-image img-fluid" alt="campaign-8">
                </div>
                <div class="body-inner">
                  <div class="entry-heading">
                    <h3 class="entry-title text-center"><span>Fusioni aziendali</span></h3>
                  </div>
                  <div class="campaign-main-style-3">
                    Fenomeni straordinari e di portata strategica quali la fusione tra due o piu' società, richiedono team manageriali esperti ed efficaci, composti da dirigenti, che hanno già gestito situazioni del genere.				            </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="item-campaign col-md-6">
              <div class="item-campaign-wrapper">
                <div class="entry-thumbnail text-center">
                  <img src="images/Fondi1.png" class="attachment-thumbnails-crowdfunding wp-post-image" alt="campaign-8">
                </div>
                <div class="body-inner">
                  <div class="entry-heading">
                    <h3 class="entry-title text-center"><span>Fondi sovrani</span></h3>
                  </div>
                  <div class="campaign-main-style-3">
                    I fondi sovrani sono alcuni speciali veicoli di investimento pubblici che appartengono direttamente ai governi dei relativi paesi, che vengono utilizzati per investire in strumenti finanziari (azioni, obbligazioni, immobili) e altre attività i surplus fiscali o le riserve di valuta estera.				            </div>
                </div>
              </div>
            </div>
            <div class="item-campaign col-md-6">
              <div class="item-campaign-wrapper">
                <div class="entry-thumbnail text-center">
                  <img src="images/angel.jpg" class="attachment-thumbnails-crowdfunding wp-post-image img-fluid" alt="campaign-8">
                </div>
                <div class="body-inner">
                  <div class="entry-heading">
                    <h3 class="entry-title text-center"><span>Distretted assett</span></h3>
                  </div>
                  <div class="campaign-main-style-3">
                    E' attivo un ulteriore segmento di business avente ad oggetto l’investimento diretto (“principal finance”) e la consulenza all’investimento in portafogli di attivi illiquidi e distressed assets.              		</div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br>
    </section>

    <section id="section3">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="divider-new text-center" style="padding: 0;margin-top:0px;">
              <h2 class="h2-responsive wow fadeInDown" style="color: white;">Companies</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <img id="popover" data-toggle="popover" data-placement="bottom" title="DECA CONSULTING SRL" data-html="true" data-content="Holding immobiliare proprietaria dell'Eracle Hotel in Selinunte (TP), due Centri Direzionali, altri vari immobili e partecipazioni di minoranza in diverse societa' sul territorio trapanese.<br>E' una controllata di Mediterraneo Capital Ltd.<br>E' attualmente in corso una operazione di Cartolarizzazione sull'intero gruppo per un valore pari a 25.000.000,00€ " src="http://www.hoteleracle.it/App_Images/0aeda559-f6fc-477b-8711-dd254c074d5e_l.jpg" class="col-md-2">
          <img id="popover2" data-toggle="popover" data-placement="bottom" title="DASAWEB LTD" data-html="true" data-content="Dasaweb Ltd e' una societa' che offre servizi professionali di hosting, housing, acquisto domini, VPN e clouding a prezzi competitivi partecipata da Mediterraneo Capital Ltd (30%).<br>Il progetto Dasaweb.net in qualita' di start-up era stato precedentemente oggetto di incubazione per 18 mesi dalla nostra Mediterraneo Advisor srl.<br><br>E' attualmente in corso una Operazione di Pre-seed Capital. Exit prevista entro Giugno 2018. " src="http://website-thumbnails.informer.com/thumbnails/280x202/d/dasaweb.net.png" class="col-md-2">
          <img id="popover3" data-toggle="popover" data-placement="bottom" title="O.R.M. SRL" data-html="true" data-content="O.R.M. srl e' una societa' del settore metalmeccanico nata nel 1966 e partecipata (11,11%) da Mediterraneo Advisor srl.<br>E' stata effettuata una operazione di Replacement. E' in fase di EXIT.<br>Nel mese di Gennaio 2017 e' stato sottoscritto contratto per cessione della partecipazione ad uno degli attuali soci di minoranza per uno valore pari all'investimento effettuato. La cessione definitiva e' prevista entro e non oltre il 30 Marzo 2017. " src="https://media.licdn.com/mpr/mpr/shrink_200_200/AAEAAQAAAAAAAATTAAAAJGE1ZWIyZjBjLTRkZjYtNDMzMC1hNThmLWVmYjdiYjc1NzgwOA.png" class="col-md-2">
          <img id="popover4" data-toggle="popover" data-placement="bottom" title="INTERLUDE MANAGEMENT SRL" data-html="true" data-content="INTERLUDE MANAGEMENT srl e' una societa' di management del settore turistico-ricettivo e partecipata (24%) da Mediterraneo Advisor srl,<br>E' stata effettuata una operazione di micro-seed. E' in fase di EXIT. " src="http://www.interludehotels.it/images/interlude-hotels-and-resort.jpg" class="col-md-2">
          <img id="popover5" data-toggle="popover" data-placement="bottom" title="Holiday99 Soc. Coop. A r.l." data-html="true" data-content="La Holiday99 e' una Holding immobiliare proprietaria dell'hotel Parco dell'Etna sito a Bronte, provincia di Catania.<br>E' attualmente in corso una operazione di Cartolarizzazione sull'intero gruppo per un valore pari a 2.500.000,00€ " src="http://img1.initalia.it/main_photo/500/25/46.jpg" class="col-md-2">
          <img id="popover6" data-toggle="popover" data-placement="bottom" title="METI CONSULTING LTD - MALTA" data-html="true" data-content="A Marzo 2017 Mediterraneo Capital ha sottoscritto contratto di investimento per l'acquisizione del 70% di Meti Consulting Ltd con sede a La Valletta (Malta). A seguito della acquisizione la societa' prendera' il nome di MEDITERRANEO CAPITAL PARTNERS LTD ed avra' la responsabilita' dello sviluppo estero del gruppo. " src="http://www.h2biz.eu/public/min_3214-i-meti.jpg" class="col-md-2">
        </div>
    </div>

    <!-- carousel -->
    <div class="container">
	  <div class="row">
	    <div class="col-md-12">
	      <div class="carousel carousel-showmanymoveone slide" id="carousel123">
	        <div class="carousel-inner">
	          <div class="item active">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/0054A6/fff/&amp;text=1" class="img-responsive"></a></div>
	          </div>
	          <div class="item">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/002d5a/fff/&amp;text=2" class="img-responsive"></a></div>
	          </div>
	          <div class="item">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/d6d6d6/333&amp;text=3" class="img-responsive"></a></div>
	          </div>          
	          <div class="item">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/002040/eeeeee&amp;text=4" class="img-responsive"></a></div>
	          </div>
	          <div class="item">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/0054A6/fff/&amp;text=5" class="img-responsive"></a></div>
	          </div>
	          <div class="item">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/002d5a/fff/&amp;text=6" class="img-responsive"></a></div>
	          </div>
	          <div class="item">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/eeeeee&amp;text=7" class="img-responsive"></a></div>
	          </div>
	          <div class="item">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/40a1ff/002040&amp;text=8" class="img-responsive"></a></div>
	          </div>
	        </div>
	        <a class="left carousel-control" href="#carousel123" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
	        <a class="right carousel-control" href="#carousel123" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
	      </div>
	    </div>
	  </div>
	  <div class="row">
	    <div class="col-md-12 text-center">
	      <h2>Second carousel test</h2>
	    </div>
	  </div>  
	  <div class="row">
	    <div class="col-md-12">
	      <div class="carousel carousel-showmanymoveone slide" id="carouselABC">
	        <div class="carousel-inner">
	          <div class="item active">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/0054A6/fff/&amp;text=A" class="img-responsive"></a></div>
	          </div>
	          <div class="item">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/002d5a/fff/&amp;text=B" class="img-responsive"></a></div>
	          </div>
	          <div class="item">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/d6d6d6/333&amp;text=C" class="img-responsive"></a></div>
	          </div>          
	          <div class="item">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/002040/eeeeee&amp;text=D" class="img-responsive"></a></div>
	          </div>
	          <div class="item">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/0054A6/fff/&amp;text=E" class="img-responsive"></a></div>
	          </div>
	          <div class="item">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/002d5a/fff/&amp;text=F" class="img-responsive"></a></div>
	          </div>
	          <div class="item">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/eeeeee&amp;text=G" class="img-responsive"></a></div>
	          </div>
	          <div class="item">
	            <div class="col-xs-12 col-sm-6 col-md-3"><a href="#"><img src="http://placehold.it/500/40a1ff/002040&amp;text=H" class="img-responsive"></a></div>
	          </div>
	        </div>
	        <a class="left carousel-control" href="#carouselABC" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
	        <a class="right carousel-control" href="#carouselABC" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
	      </div>
	    </div>
	  </div>  
	</div>
	<!-- END carousel -->
    </section>

    <footer class="wpo-footer">
        <div class="container">
            <div class="row">
              <div class="col-md-2">
                <h2 class="footer-link-head">Link utili</h2>
                <ul class="footer-link">
                  <li><a href="#">Contattaci</a></li>
                  <li><a href="#">Termini di Servizio</a></li>
                  <li><a href="#">Cookie Policy</a></li>
                  <li><a href="#">Privacy Policy</a></li>
                </ul>
              </div>
              <div class="col-md-3">
                <h2 class="footer-link-head">Social</h2>
                <ul class="footer-link">
                  <li><a href="#"><i class="fa fa-facebook"></i> MediterraneoCapital su Facebook</a></li>
                  <li><a href="#"><i class="fa fa-twitter"></i> MediterraneoCapital su Twitter</a></li>
                  <li><a href="#"><i class="fa fa-linkedin"></i> MediterraneoCapital su LinkedIn</a></li>
                </ul>
              </div>
              <div class="col-md-3">

              </div>
              <div class="col-md-4 logo">
                <p><strong>MEDITERRANEO CAPITAL LTD.</strong>
                <address>SALISBURY HOUSE 81 High Street
                Potters Bar<br>
                Hertfordshire<br>
                United Kingdom<br>
                EN6 5AS</address></p>
              </div>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="js/all.js"></script>

    <!-- carousel -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js'></script>

	<script src="js/index.js"></script>
	<!-- END carousel -->
</body>
</html>
