@extends('layout.auth_logged')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                  <form class="form-horizontal" role="form" method="POST" action="{{ url('/company/summary') }}">
                      {{ csrf_field() }}

                      <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                          <label class="col-md-4 control-label">Tipo di azienda</label>

                          <div class="col-md-8">
                            <select class="form-control" name="type" id="type">
                              <option value="0">Azienda già esistente</option>
                              <option value="1">Startup</option>
                            </select>

                              @if ($errors->has('type'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('type') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                          <label for="description" class="col-md-4 control-label">Descrizione breve<br><i>(Massimo 2 righe: Settore industriale – Segmento specifico di business)</i></label>

                          <div class="col-md-6">
                              <textarea id="description" class="form-control" name="description">{{ old('description') }}</textarea>

                              @if ($errors->has('description'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('description') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('product') ? ' has-error' : '' }}">
                          <label for="product" class="col-md-4 control-label">Prodotto/servizio<br><i>(Massimo 25 righe: Prodotto/Servizio offerto – Elementi distintivi – Beneficio per il cliente)</i></label>

                          <div class="col-md-6">
                              <textarea id="product" class="form-control" name="product">{{ old('product') }}</textarea>

                              @if ($errors->has('product'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('product') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('market') ? ' has-error' : '' }}">
                          <label for="market" class="col-md-4 control-label">Mercato<br><i>(Massimo 20 righe: Clienti target – Grandezza e previsioni di crescita del Mercato – Proprio Mercato primario di riferimento e futuri Mercati – Strategia di marketing e vendita – Definizione dei competitors – Barriere all’ingresso)</i></label>

                          <div class="col-md-6">
                              <textarea id="market" class="form-control" name="market">{{ old('market') }}</textarea>

                              @if ($errors->has('market'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('market') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('status_development') ? ' has-error' : '' }}">
                          <label for="status_development" class="col-md-4 control-label">Stadio di sviluppo dell'idea imprenditoriale</label>

                          <div class="col-md-6">
                              <select class="form-control" name="status_development" id="status_development0">
                                <option value="Seed X">Seed X</option>
                                <option value="Venture Capital">Venture Capital</option>
                                <option value="Expansion">Expansion</option>
                                <option value="Fusione aziendale">Fusione aziendale</option>
                                <option value="Acquisizione aziendale">Acquisizione aziendale</option>
                                <option value="Scissione aziendale">Scissione aziendale</option>
                                <option value="Management Buy-Out – MBO">Management Buy-Out – MBO</option>
                                <option value="Management Buy-In – MBI">Management Buy-In – MBI</option>
                                <option value="Replacement – Sostituzione soci">Replacement – Sostituzione soci</option>
                                <option value="Spin-Off">Spin-Off</option>
                                <option value="Passaggio generazionale">Passaggio generazionale</option>
                                <option value="Vuoto manageriale">Vuoto manageriale</option>
                                <option value="Sviluppo commerciale">Sviluppo commerciale</option>
                                <option value="Refinancing">Refinancing</option>
                                <option value="Internazionalizzazione">Internazionalizzazione</option>
                                <option value="Azienda in crisi">Azienda in crisi</option>
                                <option value="Quotazione segmento AIM">Quotazione segmento AIM</option>
                                <option value="Quotazione segmento MTA">Quotazione segmento MTA</option>
                                <option value="Quotazione segmento MIV">Quotazione segmento MIV</option>
                                <option value="Quotazione segmento STAR">Quotazione segmento STAR</option>
                                <option value="Quotazione SPAC">Quotazione SPAC</option>
                                <option value="Quotazione segmento EXTRAMOT (Minibond)">Quotazione segmento EXTRAMOT (Minibond)</option>
                                <option value="Finanziamento attraverso minibond">Finanziamento attraverso minibond</option>
                                <option value="Corporate governance">Corporate governance</option>
                                <option value="Cartoralizzazione immobiliare">Cartoralizzazione immobiliare</option>
                                <option value="Creazione nuovo fondo di investimento">Creazione nuovo fondo di investimento</option>
                                <option value="Creazione società in Regno Unito">Creazione società in Regno Unito</option>
                                <option value="Creazione società in Lussemburgo">Creazione società in Lussemburgo</option>
                                <option value="Creazione società in Malta">Creazione società in Malta</option>
                                <option value="Cartoralizzazione immobiliare">Cartoralizzazione immobiliare</option>
                                <option value="Cartoralizzazione immobiliare">Cartoralizzazione immobiliare</option>
                                <option value="Altro">Altro (specificare):</option>
                              </select>

                              <select class="form-control" name="status_development" id="status_development1">
                                <option value="Incubazione">Incubazione</option>
                                <option value="Pre-seed">Pre-seed</option>
                                <option value="Seed X">Seed X</option>
                                <option value="Altro">Altro (specificare):</option>
                              </select>

                              <input type="text" name="other" class="form-control" id="other">

                              @if ($errors->has('status_development'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('status_development') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                          <label for="status" class="col-md-4 control-label">Massimo 10 righe: Stadio di sviluppo del Prodotto/Servizio offerto – Penetrazione del Mercato target – Prossimi passi</label>

                          <div class="col-md-6">
                              <textarea id="status" class="form-control" name="status">{{ old('status') }}</textarea>

                              @if ($errors->has('status'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('status') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('property') ? ' has-error' : '' }}">
                          <label for="property" class="col-md-4 control-label">Proprietà intellettuale<br><i>(Massimo 5 righe: Presenza di brevetti e/o copyright – Difendibilità dell’idea di business)</i></label>

                          <div class="col-md-6">
                              <textarea id="property" class="form-control" name="property">{{ old('property') }}</textarea>

                              @if ($errors->has('property'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('property') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('managment') ? ' has-error' : '' }}">
                          <label for="managment" class="col-md-4 control-label">Managment Team<br><i>(Figure chiave del progetto di impresa e competenze: Nome – Posizione/Ruolo ricoperto – Esperienze pregresse)</i></label>

                          <div class="col-md-6">
                              <textarea id="managment" class="form-control" name="managment">{{ old('managment') }}</textarea>

                              @if ($errors->has('managment'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('managment') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('model_business') ? ' has-error' : '' }}">
                          <label for="model_business" class="col-md-4 control-label">Modello di business<br><i>(Massimo 20 righe: Modello di vendita – Modello di ricavi – Break Even analysis – Scalabilità del business in contesto internazionale)</i></label>

                          <div class="col-md-6">
                              <textarea id="model_business" class="form-control" name="model_business">{{ old('model_business') }}</textarea>

                              @if ($errors->has('model_business'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('model_business') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('swot') ? ' has-error' : '' }}">
                          <label for="swot" class="col-md-4 control-label">SWOT ANALYSIS</label>

                          <div class="col-md-6">
                              <input id="punti_di_forza" class="form-control" name="punti_di_forza" value="{{ old('punti_di_forza') }}" required>
                              <input id="punti_di_debolezza" class="form-control" name="punti_di_debolezza" value="{{ old('punti_di_debolezza') }}" required>
                              <input id="opportunita" class="form-control" name="opportunita" value="{{ old('opportunita') }}" required>
                              <input id="rischi" class="form-control" name="rischi" value="{{ old('rischi') }}" required>
                              La reale innovativita’ del business consiste <input id="innovazione" class="form-control" name="innovazione" value="{{ old('innovazione') }}" required>

                              @if ($errors->has('swot'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('swot') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('financial_information') ? ' has-error' : '' }}">
                          <label for="financial_information" class="col-md-4 control-label">Informazioni finanziarie</label>

                          <div class="col-md-6">
                              <i>FATTURATI ANNI PRECEDENTI</i>
                              <table id="prev_fatturati">
                                <thead>
                                  <tr>
                                    <th>in 000 €/a</th>
                                    <th>2012 reale</th>
                                    <th>2013 reale</th>
                                    <th>2014 reale</th>
                                    <th>2015 reale</th>
                                    <th>2015 prev</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>Fatturato</td>
                                    <td><input type="text" class="form-control" name="prec_fatturato_2012"></td>
                                    <td><input type="text" class="form-control" name="prec_fatturato_2013"></td>
                                    <td><input type="text" class="form-control" name="prec_fatturato_2014"></td>
                                    <td><input type="text" class="form-control" name="prec_fatturato_2015"></td>
                                    <td><input type="text" class="form-control" name="prec_fatturato_2015_prev"></td>
                                  </tr>
                                  <tr>
                                    <td>Costi</td>
                                    <td><input type="text" class="form-control" name="prec_costi_2012"></td>
                                    <td><input type="text" class="form-control" name="prec_costi_2013"></td>
                                    <td><input type="text" class="form-control" name="prec_costi_2014"></td>
                                    <td><input type="text" class="form-control" name="prec_costi_2015"></td>
                                    <td><input type="text" class="form-control" name="prec_costi_2015_prev"></td>
                                  </tr>
                                  <tr>
                                    <td>Net Profit</td>
                                    <td><input type="text" class="form-control" name="prec_netprofit_2012"></td>
                                    <td><input type="text" class="form-control" name="prec_netprofit_2013"></td>
                                    <td><input type="text" class="form-control" name="prec_netprofit_2014"></td>
                                    <td><input type="text" class="form-control" name="prec_netprofit_2015"></td>
                                    <td><input type="text" class="form-control" name="prec_netprofit_2015_prev"></td>
                                  </tr>
                                  <tr>
                                    <td>Clienti</td>
                                    <td><input type="text" class="form-control" name="prec_clienti_2012"></td>
                                    <td><input type="text" class="form-control" name="prec_clienti_2013"></td>
                                    <td><input type="text" class="form-control" name="prec_clienti_2014"></td>
                                    <td><input type="text" class="form-control" name="prec_clienti_2015"></td>
                                    <td><input type="text" class="form-control" name="prec_clienti_2015_prev"></td>
                                  </tr>
                                </tbody>
                              </table>
                              <br><i>FATTURATI PREVISIONALI</i>
                              <table>
                                <thead>
                                  <tr>
                                    <th>in 000 €/a</th>
                                    <th>2016 prev</th>
                                    <th>2017 prev</th>
                                    <th>2018 prev</th>
                                    <th>2019 prev</th>
                                    <th>2020 prev</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>Fatturato</td>
                                    <td><input type="text" class="form-control" name="prev_fatturato_2016"></td>
                                    <td><input type="text" class="form-control" name="prev_fatturato_2017"></td>
                                    <td><input type="text" class="form-control" name="prev_fatturato_2018"></td>
                                    <td><input type="text" class="form-control" name="prev_fatturato_2019"></td>
                                    <td><input type="text" class="form-control" name="prev_fatturato_2020_prev"></td>
                                  </tr>
                                  <tr>
                                    <td>Costi</td>
                                    <td><input type="text" class="form-control" name="prev_costi_2016"></td>
                                    <td><input type="text" class="form-control" name="prev_costi_2017"></td>
                                    <td><input type="text" class="form-control" name="prev_costi_2018"></td>
                                    <td><input type="text" class="form-control" name="prev_costi_2019"></td>
                                    <td><input type="text" class="form-control" name="prev_costi_2020_prev"></td>
                                  </tr>
                                  <tr>
                                    <td>Net Profit</td>
                                    <td><input type="text" class="form-control" name="prev_netprofit_2016"></td>
                                    <td><input type="text" class="form-control" name="prev_netprofit_2017"></td>
                                    <td><input type="text" class="form-control" name="prev_netprofit_2018"></td>
                                    <td><input type="text" class="form-control" name="prev_netprofit_2019"></td>
                                    <td><input type="text" class="form-control" name="prev_netprofit_2020_prev"></td>
                                  </tr>
                                  <tr>
                                    <td>Clienti</td>
                                    <td><input type="text" class="form-control" name="prev_clienti_2016"></td>
                                    <td><input type="text" class="form-control" name="prev_clienti_2017"></td>
                                    <td><input type="text" class="form-control" name="prev_clienti_2018"></td>
                                    <td><input type="text" class="form-control" name="prev_clienti_2019"></td>
                                    <td><input type="text" class="form-control" name="prev_clienti_2020_prev"></td>
                                  </tr>
                                </tbody>
                              </table>

                              @if ($errors->has('financial_information'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('financial_information') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('previous_investors') ? ' has-error' : '' }}">
                          <label class="col-md-4 control-label">Precedenti Investitori</label>

                          <div class="col-md-8">
                              <div class="radio">
                                <label>
                                  <input id="previous_investors0" type="radio" name="previous_investors" value="0">
                                  Si
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                  <input id="previous_investors1" type="radio" name="previous_investors" value="1">
                                  No
                                </label>
                              </div>

                              @if ($errors->has('previous_investors'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('previous_investors') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                          <label for="amount" class="col-md-4 control-label">Totale ammontare investito </label>

                          <div class="col-md-6">
                              <input id="amount" type="text" class="form-control" name="amount" value="{{ old('amount') }}" >

                              @if ($errors->has('amount'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('amount') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('pre_money') ? ' has-error' : '' }}">
                          <label for="pre_money" class="col-md-4 control-label">Pre-Money Valutation</label>

                          <div class="col-md-6">
                              <input id="pre_money" type="text" class="form-control" name="pre_money" value="{{ old('pre_money') }}" >

                              @if ($errors->has('pre_money'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('pre_money') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('equity') ? ' has-error' : '' }}">
                          <label for="equity" class="col-md-4 control-label">Investimento in equity ricercato  ( €): 0.000 da </label>

                          <div class="col-md-6">
                              <input id="equity" type="text" class="form-control" name="equity" value="{{ old('equity') }}" >

                              @if ($errors->has('equity'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('equity') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('usage') ? ' has-error' : '' }}">
                          <label for="usage" class="col-md-4 control-label">Come si intende utilizzare l’investimento </label>

                          <div class="col-md-6">
                              <input id="usage" type="text" class="form-control" name="usage" value="{{ old('usage') }}" >

                              @if ($errors->has('usage'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('usage') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('exit') ? ' has-error' : '' }}">
                          <label for="exit" class="col-md-4 control-label">Exit Strategy prevista</label>

                          <div class="col-md-6">
                              <input id="exit" type="text" class="form-control" name="exit" value="{{ old('exit') }}" >

                              @if ($errors->has('exit'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('exit') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('specifics') ? ' has-error' : '' }}">
                          <label for="specifics" class="col-md-4 control-label">Caratteristiche dell’investitore</label>

                          <div class="col-md-6">
                              Vista la particolarita’ del business e delle nuove feature che dovranno essere sviluppate, si richiede che la figura dell’investitore in questa fase possa apportare <input id="specifics" type="text" class="form-control" name="specifics" value="{{ old('specifics') }}" >

                              @if ($errors->has('specifics'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('specifics') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('why_us') ? ' has-error' : '' }}">
                          <label for="why_us" class="col-md-4 control-label">Perchè investire nella nostra idea di business</label>

                          <div class="col-md-6">
                              <textarea id="why_us" class="form-control" name="why_us">{{ old('why_us') }}</textarea>

                              @if ($errors->has('why_us'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('why_us') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              <button type="submit" class="btn btn-primary">
                                  <input type="hidden" name="company_id" value="{{ Auth::user()->id }}">
                                  Invia
                              </button>
                          </div>
                      </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script
			  src="https://code.jquery.com/jquery-3.1.1.min.js"
			  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			  crossorigin="anonymous"></script>
<script>
$(document).ready(function(){
    $("#other").hide();
    $('#status_development1').hide();
    $('#type').change(function(){
        var val = $(this).val();
        if(val === "1") {
          $('#status_development1').show();
          $('#status_development0').hide();
          $('#prev_fatturati').hide();
        } else {
          $('#status_development0').show();
          $('#status_development1').hide();
          $('#prev_fatturati').show();
        }
    });
    $("#status_development0").change(function() {
      var val = $(this).val();
      if(val === "Altro") {
          $("#other").show();
      } else {
          $("#other").hide();
      }
    });
    $("#status_development1").change(function() {
      var val = $(this).val();
      if(val === "Altro") {
          $("#other").show();
      } else {
          $("#other").hide();
      }
    });
});
</script>
@endsection
