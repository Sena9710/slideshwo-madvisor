@extends('layout.auth_logged')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

          @if(Auth::user()->type != -1)
          @if (Auth::user()->executive_status == 0)
            <div class="panel panel-warning">
          @elseif (Auth::user()->executive_status == 1)
            <div class="panel panel-danger">
          @elseif (Auth::user()->executive_status == 2)
            <div class="panel panel-success">
          @endif
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div class="col-md-7">
                      @if (Auth::user()->executive_status == 0)
                      Il tuo executive summary è in revisione, il nostro staff lo sta visionando, dunque riceverai una risposta il prima possibile!
                      @elseif (Auth::user()->executive_status == 1)
                      Il tuo executive summary è stato rifiutato!<br>
                      <strong>Motivazione:</strong> {{ Auth::user()->executive_status_reason }}
                      @elseif (Auth::user()->executive_status == 2)
                      Il tuo executive summary è stato accettato!<br>
                      @endif
                    </div>
                    <div class="col-md-3">
                      <a href="{{ url('/company/summary') }}" class="btn btn-info">Visualizza il tuo executive summary</a>
                    </div>
                </div>
            </div>
          @endif
        </div>
    </div>
</div>
@endsection
