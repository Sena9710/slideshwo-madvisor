@extends('layout.auth_logged')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Messagio: {{ $message->title }}</div>

                <div class="panel-body">
                  <div class="list-group">
                    <a href="#" class="list-group-item">
                      <h4 class="list-group-item-heading">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }} il {{ Carbon\Carbon::parse($message->date)->format('d M Y \a\l\l\e H:i') }}</h4>
                      <p class="list-group-item-text">{{ $message->message }}</p>
                      @if (file_exists(storage_path().'/app/attachments/'.$message->id.'/'))
                        @foreach(scandir(storage_path().'/app/attachments/'.$message->id.'/') as $file)
                        @continue($file == '.')
                        @continue($file == '..')
                        <a href="{{ url('/attachments/'.$message->id.'/') }}/{{ $file }}">{{ str_limit($file, 10) }}</a>
                        @endforeach
                      @endif
                    </a>
                    @forelse (App\Response::where('message', '=', $message->id)->cursor() as $response)
                    <a href="#" class="list-group-item">
                      @if ($response->sender_type == 'admin')
                      <h4 class="list-group-item-heading">Antonio Censabella il {{ Carbon\Carbon::parse($response->date)->format('d M Y \a\l\l\e H:i') }}</h4>
                      @elseif ($response->sender_type == 'advisor')
                      <h4 class="list-group-item-heading">{{ App\Advisor::where('id', '=', $response->sender)->first()->firstname }} {{ App\Advisor::where('id', '=', $response->sender)->first()->lastname }}  il {{ Carbon\Carbon::parse($response->date)->format('d M Y \a\l\l\e H:i') }}</h4>
                      @endif
                      <p class="list-group-item-text">{{ $response->response }}</p>
                      @if (file_exists(storage_path().'/app/attachments/response/'.$response->id.'/'))
                        @foreach(scandir(storage_path().'/app/attachments/response/'.$response->id.'/') as $file)
                        @continue($file == '.')
                        @continue($file == '..')
                        <a href="{{ url('/attachments/response/'.$response->id.'/') }}/{{ $file }}">{{ str_limit($file, 10) }}</a>
                        @endforeach
                      @endif
                    </a>
                    @empty

                    @endforelse
                    <a href="#" class="list-group-item">
                      <h4 class="list-group-item-heading">Rispondi</h4>
                      <form method="post" class="form-horizontal" action="{{ url('/advisor/message') }}" enctype='multipart/form-data'>
                        {{ csrf_field() }}
                        <input type="hidden" name="message" value="{{ $message->id }}">
                        <input type="hidden" name="sender" value="{{ $message->sender }}">
                        <input type="hidden" name="sender_type" value="advisor">

                        <div class="form-group{{ $errors->has('response') ? ' has-error' : '' }}">
                            <label for="response" class="col-md-4 control-label">Messaggio</label>

                            <div class="col-md-6">
                                <textarea id="response" class="form-control" name="response">{{ old('response') }}</textarea>

                                @if ($errors->has('response'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('response') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('attachments') ? ' has-error' : '' }}">
                            <label for="attachments" class="col-md-4 control-label">Allegati</label>

                            <div class="col-md-6">
                                Numero allegati: <input type="number" id="attachments" min="0" name="attachments" value="{{ old('attachments')  }}">

                                <div id="files">

                                </div>

                                @if ($errors->has('attachments'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('attachments') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Invia</button>
                          </div>
                        </div>
                      </form>
                    </a>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script
			  src="https://code.jquery.com/jquery-3.1.1.min.js"
			  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			  crossorigin="anonymous"></script>
<script>
$(document).ready(function(){
  var value = 0;
  $("[type='number']").keypress(function (evt) {
    evt.preventDefault();
  });
  $('#attachments').on("change paste keyup", function() {
    if(value < 0) return;
    if($('#attachments').val() > value) {
      document.getElementById('files').innerHTML += '<input type="file" name="files[]">';
    } else {
      document.getElementById('files').innerHTML = document.getElementById('files').innerHTML.substring(0, document.getElementById('files').innerHTML.length - 32);
    }
    value = $('#attachments').val();
  });
});
</script>
@endsection
