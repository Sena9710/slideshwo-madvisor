@extends('layout.auth_logged')

@section('content')
<div class="container">
    @if (session('message'))
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="alert alert-success">
              {{ session('message') }}
          </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in as Advisor!
                </div>
            </div>
        </div>
    </div>
    @if (Auth::user()->manager == '1')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Messaggi</div>

                <div class="panel-body">
                    <a href="{{ url('/advisor/message/create') }}" class="btn btn-success">Scrivi messaggio</a>

                    <table>
                      <thead>
                        <tr>
                          <th class="col-md-1">#</th>
                          <th class="col-md-5">Titolo</th>
                          <th class="col-md-4">Inviato a</th>
                          <th class="col-md-2"></th>
                        </tr>
                      </thead>
                      <tbody>
                        @forelse (App\Message::where('sender', '=', Auth::user()->id)->cursor() as $message)
                          <tr>
                            <td>{{ $message->id }}</td>
                            <td>{{ $message->title }}</td>
                            @if ($message->receiver_type == 'admin')
                            <td>Antonio Censabella</td>
                            @endif
                            <td><a href="{{ url('/advisor/message') }}/{{ $message->id }}" class="btn btn-primary">Apri &raquo;</a></td>
                          </tr>
                        @empty
                          <tr>
                            <td colspan="4"><strong>Non è presente alcun messaggio!</strong></td>
                          </tr>
                        @endforelse
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Messaggi</div>

                <div class="panel-body">
                    <a href="{{ url('/advisor/message/create') }}" class="btn btn-success">Scrivi messaggio</a>

                    <table>
                      <thead>
                        <tr>
                          <th class="col-md-1">#</th>
                          <th class="col-md-5">Titolo</th>
                          <th class="col-md-4">Inviato a</th>
                          <th class="col-md-2"></th>
                        </tr>
                      </thead>
                      <tbody>
                        @forelse (App\Message::where('sender', '=', Auth::user()->id)->cursor() as $message)
                          <tr>
                            <td>{{ $message->id }}</td>
                            <td>{{ $message->title }}</td>
                            @if ($message->receiver_type == 'admin')
                            <td>Antonio Censabella</td>
                            @endif
                            <td><a href="{{ url('/advisor/message') }}/{{ $message->id }}" class="btn btn-primary">Apri &raquo;</a></td>
                          </tr>
                        @empty
                          <tr>
                            <td colspan="4"><strong>Non è presente alcun messaggio!</strong></td>
                          </tr>
                        @endforelse
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
