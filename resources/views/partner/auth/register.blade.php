@extends('partner.layout.auth')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div style="text-align: center;"><span class="inset span"> REGISTRAZIONE </span></div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/partner/register') }}">
                        {{ csrf_field() }}
                      <div class="col-md-6">
                        <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                            <label for="firstname" class="col-md-4 control-label">Nome</label>

                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" autofocus>

                                @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <label for="lastname" class="col-md-4 control-label">Cognome</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" autofocus>

                                @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sex') ? ' has-error' : '' }}">
                            <label for="sex" class="col-md-4 control-label">Sesso</label>

                            <div class="col-md-6">
                                <select class="form-control" name="sex" id="sex">
                                  <option value="0" {{ (old('sex') == 0) ? ('selected') : ('') }}>Uomo</option>
                                  <option value="1" {{ (old('sex') == 1) ? ('selected') : ('') }}>Donna</option>
                                </select>

                                @if ($errors->has('sex'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sex') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <label for="city" class="col-md-4 control-label">Città</label>

                            <div class="col-md-6">
                                <select class="form-control" name="city" id="city">
                                  <option value="Agrigento">Agrigento</option>
                                  <option value="Alessandria">Alessandria</option>
                                  <option value="Ancona">Ancona</option>
                                  <option value="Aosta">Aosta</option>
                                  <option value="Arezzo">Arezzo</option>
                                  <option value="Ascoli Piceno">Ascoli Piceno</option>
                                  <option value="Asti">Asti</option>
                                  <option value="Avellino">Avellino</option>
                                  <option value="Bari">Bari</option>
                                  <option value="Barletta-Andria-Trani">Barletta-Andria-Trani</option>
                                  <option value="Belluno">Belluno</option>
                                  <option value="Benevento">Benevento</option>
                                  <option value="Bergamo">Bergamo</option>
                                  <option value="Biella">Biella</option>
                                  <option value="Bologna">Bologna</option>
                                  <option value="Bolzano">Bolzano</option>
                                  <option value="Brescia">Brescia</option>
                                  <option value="Brindisi">Brindisi</option>
                                  <option value="Cagliari">Cagliari</option>
                                  <option value="Caltanissetta">Caltanissetta</option>
                                  <option value="Campobasso">Campobasso</option>
                                  <option value="Carbonia-Iglesias">Carbonia-Iglesias</option>
                                  <option value="Caserta">Caserta</option>
                                  <option value="Catania">Catania</option>
                                  <option value="Catanzaro">Catanzaro</option>
                                  <option value="Chieti">Chieti</option>
                                  <option value="Como">Como</option>
                                  <option value="Cosenza">Cosenza</option>
                                  <option value="Cremona">Cremona</option>
                                  <option value="Crotone">Crotone</option>
                                  <option value="Cuneo">Cuneo</option>
                                  <option value="Enna">Enna</option>
                                  <option value="Fermo">Fermo</option>
                                  <option value="Ferrara">Ferrara</option>
                                  <option value="Firenze">Firenze</option>
                                  <option value="Foggia">Foggia</option>
                                  <option value="Forlì-Cesena">Forlì-Cesena</option>
                                  <option value="Frosinone">Frosinone</option>
                                  <option value="Genova">Genova</option>
                                  <option value="Gorizia">Gorizia</option>
                                  <option value="Grosseto">Grosseto</option>
                                  <option value="Imperia">Imperia</option>
                                  <option value="Isernia">Isernia</option>
                                  <option value="L'Aquila">L'Aquila</option>
                                  <option value="La Spezia">La Spezia</option>
                                  <option value="Latina">Latina</option>
                                  <option value="Lecce">Lecce</option>
                                  <option value="Lecco">Lecco</option>
                                  <option value="Livorno">Livorno</option>
                                  <option value="Lodi">Lodi</option>
                                  <option value="Lucca">Lucca</option>
                                  <option value="Macerata">Macerata</option>
                                  <option value="Mantova">Mantova</option>
                                  <option value="Massa e Carrara">Massa e Carrara</option>
                                  <option value="Matera">Matera</option>
                                  <option value="Medio Campidano">Medio Campidano</option>
                                  <option value="Messina">Messina</option>
                                  <option value="Milano">Milano</option>
                                  <option value="Modena">Modena</option>
                                  <option value="Monza e Brianza">Monza e Brianza</option>
                                  <option value="Napoli">Napoli</option>
                                  <option value="Novara">Novara</option>
                                  <option value="Nuoro">Nuoro</option>
                                  <option value="Ogliastra">Ogliastra</option>
                                  <option value="Olbia-Tempio">Olbia-Tempio</option>
                                  <option value="Oristano">Oristano</option>
                                  <option value="Padova">Padova</option>
                                  <option value="Palermo">Palermo</option>
                                  <option value="Parma">Parma</option>
                                  <option value="Pavia">Pavia</option>
                                  <option value="Perugia">Perugia</option>
                                  <option value="Pesaro e Urbino">Pesaro e Urbino</option>
                                  <option value="Pescara">Pescara</option>
                                  <option value="Piacenza">Piacenza</option>
                                  <option value="Pisa">Pisa</option>
                                  <option value="Pistoia">Pistoia</option>
                                  <option value="Pordenone">Pordenone</option>
                                  <option value="Potenza">Potenza</option>
                                  <option value="Prato">Prato</option>
                                  <option value="Ragusa">Ragusa</option>
                                  <option value="Ravenna">Ravenna</option>
                                  <option value="Reggio Calabria">Reggio Calabria</option>
                                  <option value="Reggio Emilia">Reggio Emilia</option>
                                  <option value="Rieti">Rieti</option>
                                  <option value="Rimini">Rimini</option>
                                  <option value="Roma">Roma</option>
                                  <option value="Rovigo">Rovigo</option>
                                  <option value="Salerno">Salerno</option>
                                  <option value="Sassari">Sassari</option>
                                  <option value="Savona">Savona</option>
                                  <option value="Siena">Siena</option>
                                  <option value="Siracusa">Siracusa</option>
                                  <option value="Sondrio">Sondrio</option>
                                  <option value="Taranto">Taranto</option>
                                  <option value="Teramo">Teramo</option>
                                  <option value="Terni">Terni</option>
                                  <option value="Torino">Torino</option>
                                  <option value="Trapani">Trapani</option>
                                  <option value="Trento">Trento</option>
                                  <option value="Treviso">Treviso</option>
                                  <option value="Trieste">Trieste</option>
                                  <option value="Udine">Udine</option>
                                  <option value="Varese">Varese</option>
                                  <option value="Venezia">Venezia</option>
                                  <option value="Verbano-Cusio-Ossola">Verbano-Cusio-Ossola</option>
                                  <option value="Vercelli">Vercelli</option>
                                  <option value="Verona">Verona</option>
                                  <option value="Vibo Valentia">Vibo Valentia</option>
                                  <option value="Vicenza">Vicenza</option>
                                  <option value="Viterbo">Viterbo</option>
                                </select>

                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('province') ? ' has-error' : '' }}">
                            <label for="province" class="col-md-4 control-label">Provincia</label>

                            <div class="col-md-6">
                                <select class="form-control" name="province" id="province">
                                  <option value="Agrigento">Agrigento</option>
                                  <option value="Alessandria">Alessandria</option>
                                  <option value="Ancona">Ancona</option>
                                  <option value="Aosta">Aosta</option>
                                  <option value="Arezzo">Arezzo</option>
                                  <option value="Ascoli Piceno">Ascoli Piceno</option>
                                  <option value="Asti">Asti</option>
                                  <option value="Avellino">Avellino</option>
                                  <option value="Bari">Bari</option>
                                  <option value="Barletta-Andria-Trani">Barletta-Andria-Trani</option>
                                  <option value="Belluno">Belluno</option>
                                  <option value="Benevento">Benevento</option>
                                  <option value="Bergamo">Bergamo</option>
                                  <option value="Biella">Biella</option>
                                  <option value="Bologna">Bologna</option>
                                  <option value="Bolzano">Bolzano</option>
                                  <option value="Brescia">Brescia</option>
                                  <option value="Brindisi">Brindisi</option>
                                  <option value="Cagliari">Cagliari</option>
                                  <option value="Caltanissetta">Caltanissetta</option>
                                  <option value="Campobasso">Campobasso</option>
                                  <option value="Carbonia-Iglesias">Carbonia-Iglesias</option>
                                  <option value="Caserta">Caserta</option>
                                  <option value="Catania">Catania</option>
                                  <option value="Catanzaro">Catanzaro</option>
                                  <option value="Chieti">Chieti</option>
                                  <option value="Como">Como</option>
                                  <option value="Cosenza">Cosenza</option>
                                  <option value="Cremona">Cremona</option>
                                  <option value="Crotone">Crotone</option>
                                  <option value="Cuneo">Cuneo</option>
                                  <option value="Enna">Enna</option>
                                  <option value="Fermo">Fermo</option>
                                  <option value="Ferrara">Ferrara</option>
                                  <option value="Firenze">Firenze</option>
                                  <option value="Foggia">Foggia</option>
                                  <option value="Forlì-Cesena">Forlì-Cesena</option>
                                  <option value="Frosinone">Frosinone</option>
                                  <option value="Genova">Genova</option>
                                  <option value="Gorizia">Gorizia</option>
                                  <option value="Grosseto">Grosseto</option>
                                  <option value="Imperia">Imperia</option>
                                  <option value="Isernia">Isernia</option>
                                  <option value="L'Aquila">L'Aquila</option>
                                  <option value="La Spezia">La Spezia</option>
                                  <option value="Latina">Latina</option>
                                  <option value="Lecce">Lecce</option>
                                  <option value="Lecco">Lecco</option>
                                  <option value="Livorno">Livorno</option>
                                  <option value="Lodi">Lodi</option>
                                  <option value="Lucca">Lucca</option>
                                  <option value="Macerata">Macerata</option>
                                  <option value="Mantova">Mantova</option>
                                  <option value="Massa e Carrara">Massa e Carrara</option>
                                  <option value="Matera">Matera</option>
                                  <option value="Medio Campidano">Medio Campidano</option>
                                  <option value="Messina">Messina</option>
                                  <option value="Milano">Milano</option>
                                  <option value="Modena">Modena</option>
                                  <option value="Monza e Brianza">Monza e Brianza</option>
                                  <option value="Napoli">Napoli</option>
                                  <option value="Novara">Novara</option>
                                  <option value="Nuoro">Nuoro</option>
                                  <option value="Ogliastra">Ogliastra</option>
                                  <option value="Olbia-Tempio">Olbia-Tempio</option>
                                  <option value="Oristano">Oristano</option>
                                  <option value="Padova">Padova</option>
                                  <option value="Palermo">Palermo</option>
                                  <option value="Parma">Parma</option>
                                  <option value="Pavia">Pavia</option>
                                  <option value="Perugia">Perugia</option>
                                  <option value="Pesaro e Urbino">Pesaro e Urbino</option>
                                  <option value="Pescara">Pescara</option>
                                  <option value="Piacenza">Piacenza</option>
                                  <option value="Pisa">Pisa</option>
                                  <option value="Pistoia">Pistoia</option>
                                  <option value="Pordenone">Pordenone</option>
                                  <option value="Potenza">Potenza</option>
                                  <option value="Prato">Prato</option>
                                  <option value="Ragusa">Ragusa</option>
                                  <option value="Ravenna">Ravenna</option>
                                  <option value="Reggio Calabria">Reggio Calabria</option>
                                  <option value="Reggio Emilia">Reggio Emilia</option>
                                  <option value="Rieti">Rieti</option>
                                  <option value="Rimini">Rimini</option>
                                  <option value="Roma">Roma</option>
                                  <option value="Rovigo">Rovigo</option>
                                  <option value="Salerno">Salerno</option>
                                  <option value="Sassari">Sassari</option>
                                  <option value="Savona">Savona</option>
                                  <option value="Siena">Siena</option>
                                  <option value="Siracusa">Siracusa</option>
                                  <option value="Sondrio">Sondrio</option>
                                  <option value="Taranto">Taranto</option>
                                  <option value="Teramo">Teramo</option>
                                  <option value="Terni">Terni</option>
                                  <option value="Torino">Torino</option>
                                  <option value="Trapani">Trapani</option>
                                  <option value="Trento">Trento</option>
                                  <option value="Treviso">Treviso</option>
                                  <option value="Trieste">Trieste</option>
                                  <option value="Udine">Udine</option>
                                  <option value="Varese">Varese</option>
                                  <option value="Venezia">Venezia</option>
                                  <option value="Verbano-Cusio-Ossola">Verbano-Cusio-Ossola</option>
                                  <option value="Vercelli">Vercelli</option>
                                  <option value="Verona">Verona</option>
                                  <option value="Vibo Valentia">Vibo Valentia</option>
                                  <option value="Vicenza">Vicenza</option>
                                  <option value="Viterbo">Viterbo</option>
                                </select>

                                @if ($errors->has('province'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('province') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('zipcode') ? ' has-error' : '' }}">
                            <label for="zipcode" class="col-md-4 control-label">CAP</label>

                            <div class="col-md-6">
                                <input id="zipcode" type="text" class="form-control" name="zipcode" value="{{ old('zipcode') }}">

                                @if ($errors->has('zipcode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('zipcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                            <label for="street" class="col-md-4 control-label">Indirizzo</label>

                            <div class="col-md-6">
                                <input id="street" type="text" class="form-control" name="street" value="{{ old('street') }}">

                                @if ($errors->has('street'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('street') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                            <label for="dob" class="col-md-4 control-label">Data di nascita</label>

                            <div class="col-md-6">
                                <input id="dob" type="text" pattern="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d" class="form-control" name="dob" placeholder="GG/MM/AAAA" value="{{ old('dob') }}">

                                @if ($errors->has('dob'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('cellphone') ? ' has-error' : '' }}">
                            <label for="cellphone" class="col-md-4 control-label">Cellulare</label>

                            <div class="col-md-6">
                                <input id="cellphone" type="text" class="form-control" name="cellphone" value="{{ old('cellphone') }}">

                                @if ($errors->has('cellphone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cellphone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class="col-md-4 control-label">Tipo</label>

                            <div class="col-md-6">
                                <select class="form-control" name="type">
                                  <option value="0">Investitori seriali</option>
                                  <option value="1">Business Angels</option>
                                  <option value="2">Venture Incubators</option>
                                  <option value="3">Fondi di pre-seed</option>
                                  <option value="4">Fondi di Seed</option>
                                  <option value="5">Fondi di Venture Capital</option>
                                  <option value="6">Fondi di Private Equity</option>
                                  <option value="7">Fondi di Private Debt</option>
                                  <option value="8">Fondi di Turnaround</option>
                                  <option value="9">Fondi di Buy-Out</option>
                                  <option value="10">Fondi di Replacement</option>
                                  <option value="11">Fondi di Passaggio Generazionale</option>
                                  <option value="12">Fondi di Vuoto Manageriale</option>
                                  <option value="13">Fondi Sovrani</option>
                                  <option value="14">Holding</option>
                                  <option value="15">Fondi Immobiliari</option>
                                  <option value="16">Borse</option>
                                  <option value="17">Investitori industriali</option>
                                  <option value="18">Azienda target</option>
                                </select>

                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                            <label for="website" class="col-md-4 control-label">Sito Web</label>

                            <div class="col-md-6">
                                <input id="website" type="text" class="form-control" name="website" value="{{ old('website') }}">

                                @if ($errors->has('website'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      
                        <div class="form-group{{ $errors->has('contact') ? ' has-error' : '' }}">
                            <label for="contact" class="col-md-4 control-label">Contatto</label>

                            <div class="col-md-6">
                                <input id="contact" type="text" class="form-control" name="contact" value="{{ old('contact') }}">

                                @if ($errors->has('contact'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contact') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group{{ $errors->has('geographical') ? ' has-error' : '' }}">
                            <label for="geographical" class="col-md-4 control-label">Focus geografico</label>

                            <div class="col-md-6">
                                <input id="geographical" type="text" class="form-control" name="geographical" value="{{ old('geographical') }}">

                                @if ($errors->has('geographical'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('geographical') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sector') ? ' has-error' : '' }}">
                            <label for="sector" class="col-md-4 control-label">Settore</label>

                            <div class="col-md-6">
                                <input id="sector" type="text" class="form-control" name="sector" value="{{ old('sector') }}">

                                @if ($errors->has('sector'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sector') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('investment') ? ' has-error' : '' }}">
                            <label for="investment" class="col-md-4 control-label">Focus di investimento</label>

                            <div class="col-md-6">
                                <input id="investment" type="text" class="form-control" name="investment" value="{{ old('investment') }}">

                                @if ($errors->has('investment'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('investment') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('minimum') ? ' has-error' : '' }}">
                            <label for="minimum" class="col-md-4 control-label">Investimento minimo</label>

                            <div class="col-md-6">
                                <input id="minimum" type="text" class="form-control" name="minimum" value="{{ old('minimum') }}">

                                @if ($errors->has('minimum'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('minimum') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('maximum') ? ' has-error' : '' }}">
                            <label for="maximum" class="col-md-4 control-label">Investimento massimo</label>

                            <div class="col-md-6">
                                <input id="maximum" type="text" class="form-control" name="maximum" value="{{ old('maximum') }}">

                                @if ($errors->has('maximum'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('maximum') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('avg') ? ' has-error' : '' }}">
                            <label for="avg" class="col-md-4 control-label">Investimento medio</label>

                            <div class="col-md-6">
                                <input id="avg" type="text" class="form-control" name="avg" value="{{ old('avg') }}">

                                @if ($errors->has('avg'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('avg') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('executives') ? ' has-error' : '' }}">
                            <label for="executives" class="col-md-4 control-label">Numero di Executives</label>

                            <div class="col-md-6">
                                <input id="executives" type="number" class="form-control" name="executives" value="{{ old('executives') }}">

                                @if ($errors->has('executives'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('executives') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('funds') ? ' has-error' : '' }}">
                            <label for="funds" class="col-md-4 control-label">Numero di fondi</label>

                            <div class="col-md-6">
                                <input id="funds" type="text" class="form-control" name="funds" value="{{ old('funds') }}">

                                @if ($errors->has('funds'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('funds') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('capitals') ? ' has-error' : '' }}">
                            <label for="capitals" class="col-md-4 control-label">Totale capitale gestito</label>

                            <div class="col-md-6">
                                <input id="capitals" type="text" class="form-control" name="capitals" value="{{ old('capitals') }}">

                                @if ($errors->has('capitals'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('capitals') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('companies') ? ' has-error' : '' }}">
                            <label for="companies" class="col-md-4 control-label">Numero di compagnie in portfolio</label>

                            <div class="col-md-6">
                                <input id="companies" type="text" class="form-control" name="companies" value="{{ old('companies') }}">

                                @if ($errors->has('companies'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('companies') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('exits') ? ' has-error' : '' }}">
                            <label for="exits" class="col-md-4 control-label">Numero di Exit effettuate</label>

                            <div class="col-md-6">
                                <input id="exits" type="text" class="form-control" name="exits" value="{{ old('exits') }}">

                                @if ($errors->has('exits'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('exits') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('avg_funds') ? ' has-error' : '' }}">
                            <label for="avg_funds" class="col-md-4 control-label">Rendimento medio dei fondi</label>

                            <div class="col-md-6">
                                <input id="avg_funds" type="text" class="form-control" name="avg_funds" value="{{ old('avg_funds') }}">

                                @if ($errors->has('avg_funds'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('avg_funds') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      </div>
                        <div class="form-group">
                            <div class="col-md-12" style="text-align: center;">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
