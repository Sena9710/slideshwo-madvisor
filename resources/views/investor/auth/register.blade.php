@extends('investor.layout.auth')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div style="text-align: center;"><span class="inset span"> REGISTRAZIONE </span></div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/investor/register') }}">
                        {{ csrf_field() }}
                      <div class="col-md-6">
                        <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                            <label for="firstname" class="col-md-4 control-label">Nome</label>

                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" autofocus>

                                @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <label for="lastname" class="col-md-4 control-label">Cognome</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" autofocus>

                                @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sex') ? ' has-error' : '' }}">
                            <label for="sex" class="col-md-4 control-label">Sesso</label>

                            <div class="col-md-6">
                                <select class="form-control" name="sex" id="sex">
                                  <option value="0" {{ (old('sex') == 0) ? ('selected') : ('') }}>Uomo</option>
                                  <option value="1" {{ (old('sex') == 1) ? ('selected') : ('') }}>Donna</option>
                                </select>

                                @if ($errors->has('sex'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sex') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <label for="city" class="col-md-4 control-label">Città</label>

                            <div class="col-md-6">
                                <select class="form-control" name="city" id="city">
                                  <option value="Agrigento">Agrigento</option>
                                  <option value="Alessandria">Alessandria</option>
                                  <option value="Ancona">Ancona</option>
                                  <option value="Aosta">Aosta</option>
                                  <option value="Arezzo">Arezzo</option>
                                  <option value="Ascoli Piceno">Ascoli Piceno</option>
                                  <option value="Asti">Asti</option>
                                  <option value="Avellino">Avellino</option>
                                  <option value="Bari">Bari</option>
                                  <option value="Barletta-Andria-Trani">Barletta-Andria-Trani</option>
                                  <option value="Belluno">Belluno</option>
                                  <option value="Benevento">Benevento</option>
                                  <option value="Bergamo">Bergamo</option>
                                  <option value="Biella">Biella</option>
                                  <option value="Bologna">Bologna</option>
                                  <option value="Bolzano">Bolzano</option>
                                  <option value="Brescia">Brescia</option>
                                  <option value="Brindisi">Brindisi</option>
                                  <option value="Cagliari">Cagliari</option>
                                  <option value="Caltanissetta">Caltanissetta</option>
                                  <option value="Campobasso">Campobasso</option>
                                  <option value="Carbonia-Iglesias">Carbonia-Iglesias</option>
                                  <option value="Caserta">Caserta</option>
                                  <option value="Catania">Catania</option>
                                  <option value="Catanzaro">Catanzaro</option>
                                  <option value="Chieti">Chieti</option>
                                  <option value="Como">Como</option>
                                  <option value="Cosenza">Cosenza</option>
                                  <option value="Cremona">Cremona</option>
                                  <option value="Crotone">Crotone</option>
                                  <option value="Cuneo">Cuneo</option>
                                  <option value="Enna">Enna</option>
                                  <option value="Fermo">Fermo</option>
                                  <option value="Ferrara">Ferrara</option>
                                  <option value="Firenze">Firenze</option>
                                  <option value="Foggia">Foggia</option>
                                  <option value="Forlì-Cesena">Forlì-Cesena</option>
                                  <option value="Frosinone">Frosinone</option>
                                  <option value="Genova">Genova</option>
                                  <option value="Gorizia">Gorizia</option>
                                  <option value="Grosseto">Grosseto</option>
                                  <option value="Imperia">Imperia</option>
                                  <option value="Isernia">Isernia</option>
                                  <option value="L'Aquila">L'Aquila</option>
                                  <option value="La Spezia">La Spezia</option>
                                  <option value="Latina">Latina</option>
                                  <option value="Lecce">Lecce</option>
                                  <option value="Lecco">Lecco</option>
                                  <option value="Livorno">Livorno</option>
                                  <option value="Lodi">Lodi</option>
                                  <option value="Lucca">Lucca</option>
                                  <option value="Macerata">Macerata</option>
                                  <option value="Mantova">Mantova</option>
                                  <option value="Massa e Carrara">Massa e Carrara</option>
                                  <option value="Matera">Matera</option>
                                  <option value="Medio Campidano">Medio Campidano</option>
                                  <option value="Messina">Messina</option>
                                  <option value="Milano">Milano</option>
                                  <option value="Modena">Modena</option>
                                  <option value="Monza e Brianza">Monza e Brianza</option>
                                  <option value="Napoli">Napoli</option>
                                  <option value="Novara">Novara</option>
                                  <option value="Nuoro">Nuoro</option>
                                  <option value="Ogliastra">Ogliastra</option>
                                  <option value="Olbia-Tempio">Olbia-Tempio</option>
                                  <option value="Oristano">Oristano</option>
                                  <option value="Padova">Padova</option>
                                  <option value="Palermo">Palermo</option>
                                  <option value="Parma">Parma</option>
                                  <option value="Pavia">Pavia</option>
                                  <option value="Perugia">Perugia</option>
                                  <option value="Pesaro e Urbino">Pesaro e Urbino</option>
                                  <option value="Pescara">Pescara</option>
                                  <option value="Piacenza">Piacenza</option>
                                  <option value="Pisa">Pisa</option>
                                  <option value="Pistoia">Pistoia</option>
                                  <option value="Pordenone">Pordenone</option>
                                  <option value="Potenza">Potenza</option>
                                  <option value="Prato">Prato</option>
                                  <option value="Ragusa">Ragusa</option>
                                  <option value="Ravenna">Ravenna</option>
                                  <option value="Reggio Calabria">Reggio Calabria</option>
                                  <option value="Reggio Emilia">Reggio Emilia</option>
                                  <option value="Rieti">Rieti</option>
                                  <option value="Rimini">Rimini</option>
                                  <option value="Roma">Roma</option>
                                  <option value="Rovigo">Rovigo</option>
                                  <option value="Salerno">Salerno</option>
                                  <option value="Sassari">Sassari</option>
                                  <option value="Savona">Savona</option>
                                  <option value="Siena">Siena</option>
                                  <option value="Siracusa">Siracusa</option>
                                  <option value="Sondrio">Sondrio</option>
                                  <option value="Taranto">Taranto</option>
                                  <option value="Teramo">Teramo</option>
                                  <option value="Terni">Terni</option>
                                  <option value="Torino">Torino</option>
                                  <option value="Trapani">Trapani</option>
                                  <option value="Trento">Trento</option>
                                  <option value="Treviso">Treviso</option>
                                  <option value="Trieste">Trieste</option>
                                  <option value="Udine">Udine</option>
                                  <option value="Varese">Varese</option>
                                  <option value="Venezia">Venezia</option>
                                  <option value="Verbano-Cusio-Ossola">Verbano-Cusio-Ossola</option>
                                  <option value="Vercelli">Vercelli</option>
                                  <option value="Verona">Verona</option>
                                  <option value="Vibo Valentia">Vibo Valentia</option>
                                  <option value="Vicenza">Vicenza</option>
                                  <option value="Viterbo">Viterbo</option>
                                </select>

                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('province') ? ' has-error' : '' }}">
                            <label for="province" class="col-md-4 control-label">Provincia</label>

                            <div class="col-md-6">
                                <select class="form-control" name="province" id="province">
                                  <option value="Agrigento">Agrigento</option>
                                  <option value="Alessandria">Alessandria</option>
                                  <option value="Ancona">Ancona</option>
                                  <option value="Aosta">Aosta</option>
                                  <option value="Arezzo">Arezzo</option>
                                  <option value="Ascoli Piceno">Ascoli Piceno</option>
                                  <option value="Asti">Asti</option>
                                  <option value="Avellino">Avellino</option>
                                  <option value="Bari">Bari</option>
                                  <option value="Barletta-Andria-Trani">Barletta-Andria-Trani</option>
                                  <option value="Belluno">Belluno</option>
                                  <option value="Benevento">Benevento</option>
                                  <option value="Bergamo">Bergamo</option>
                                  <option value="Biella">Biella</option>
                                  <option value="Bologna">Bologna</option>
                                  <option value="Bolzano">Bolzano</option>
                                  <option value="Brescia">Brescia</option>
                                  <option value="Brindisi">Brindisi</option>
                                  <option value="Cagliari">Cagliari</option>
                                  <option value="Caltanissetta">Caltanissetta</option>
                                  <option value="Campobasso">Campobasso</option>
                                  <option value="Carbonia-Iglesias">Carbonia-Iglesias</option>
                                  <option value="Caserta">Caserta</option>
                                  <option value="Catania">Catania</option>
                                  <option value="Catanzaro">Catanzaro</option>
                                  <option value="Chieti">Chieti</option>
                                  <option value="Como">Como</option>
                                  <option value="Cosenza">Cosenza</option>
                                  <option value="Cremona">Cremona</option>
                                  <option value="Crotone">Crotone</option>
                                  <option value="Cuneo">Cuneo</option>
                                  <option value="Enna">Enna</option>
                                  <option value="Fermo">Fermo</option>
                                  <option value="Ferrara">Ferrara</option>
                                  <option value="Firenze">Firenze</option>
                                  <option value="Foggia">Foggia</option>
                                  <option value="Forlì-Cesena">Forlì-Cesena</option>
                                  <option value="Frosinone">Frosinone</option>
                                  <option value="Genova">Genova</option>
                                  <option value="Gorizia">Gorizia</option>
                                  <option value="Grosseto">Grosseto</option>
                                  <option value="Imperia">Imperia</option>
                                  <option value="Isernia">Isernia</option>
                                  <option value="L'Aquila">L'Aquila</option>
                                  <option value="La Spezia">La Spezia</option>
                                  <option value="Latina">Latina</option>
                                  <option value="Lecce">Lecce</option>
                                  <option value="Lecco">Lecco</option>
                                  <option value="Livorno">Livorno</option>
                                  <option value="Lodi">Lodi</option>
                                  <option value="Lucca">Lucca</option>
                                  <option value="Macerata">Macerata</option>
                                  <option value="Mantova">Mantova</option>
                                  <option value="Massa e Carrara">Massa e Carrara</option>
                                  <option value="Matera">Matera</option>
                                  <option value="Medio Campidano">Medio Campidano</option>
                                  <option value="Messina">Messina</option>
                                  <option value="Milano">Milano</option>
                                  <option value="Modena">Modena</option>
                                  <option value="Monza e Brianza">Monza e Brianza</option>
                                  <option value="Napoli">Napoli</option>
                                  <option value="Novara">Novara</option>
                                  <option value="Nuoro">Nuoro</option>
                                  <option value="Ogliastra">Ogliastra</option>
                                  <option value="Olbia-Tempio">Olbia-Tempio</option>
                                  <option value="Oristano">Oristano</option>
                                  <option value="Padova">Padova</option>
                                  <option value="Palermo">Palermo</option>
                                  <option value="Parma">Parma</option>
                                  <option value="Pavia">Pavia</option>
                                  <option value="Perugia">Perugia</option>
                                  <option value="Pesaro e Urbino">Pesaro e Urbino</option>
                                  <option value="Pescara">Pescara</option>
                                  <option value="Piacenza">Piacenza</option>
                                  <option value="Pisa">Pisa</option>
                                  <option value="Pistoia">Pistoia</option>
                                  <option value="Pordenone">Pordenone</option>
                                  <option value="Potenza">Potenza</option>
                                  <option value="Prato">Prato</option>
                                  <option value="Ragusa">Ragusa</option>
                                  <option value="Ravenna">Ravenna</option>
                                  <option value="Reggio Calabria">Reggio Calabria</option>
                                  <option value="Reggio Emilia">Reggio Emilia</option>
                                  <option value="Rieti">Rieti</option>
                                  <option value="Rimini">Rimini</option>
                                  <option value="Roma">Roma</option>
                                  <option value="Rovigo">Rovigo</option>
                                  <option value="Salerno">Salerno</option>
                                  <option value="Sassari">Sassari</option>
                                  <option value="Savona">Savona</option>
                                  <option value="Siena">Siena</option>
                                  <option value="Siracusa">Siracusa</option>
                                  <option value="Sondrio">Sondrio</option>
                                  <option value="Taranto">Taranto</option>
                                  <option value="Teramo">Teramo</option>
                                  <option value="Terni">Terni</option>
                                  <option value="Torino">Torino</option>
                                  <option value="Trapani">Trapani</option>
                                  <option value="Trento">Trento</option>
                                  <option value="Treviso">Treviso</option>
                                  <option value="Trieste">Trieste</option>
                                  <option value="Udine">Udine</option>
                                  <option value="Varese">Varese</option>
                                  <option value="Venezia">Venezia</option>
                                  <option value="Verbano-Cusio-Ossola">Verbano-Cusio-Ossola</option>
                                  <option value="Vercelli">Vercelli</option>
                                  <option value="Verona">Verona</option>
                                  <option value="Vibo Valentia">Vibo Valentia</option>
                                  <option value="Vicenza">Vicenza</option>
                                  <option value="Viterbo">Viterbo</option>
                                </select>

                                @if ($errors->has('province'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('province') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('zipcode') ? ' has-error' : '' }}">
                            <label for="zipcode" class="col-md-4 control-label">CAP</label>

                            <div class="col-md-6">
                                <input id="zipcode" type="text" class="form-control" name="zipcode" value="{{ old('zipcode') }}">

                                @if ($errors->has('zipcode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('zipcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                            <label for="street" class="col-md-4 control-label">Indirizzo</label>

                            <div class="col-md-6">
                                <input id="street" type="text" class="form-control" name="street" value="{{ old('street') }}">

                                @if ($errors->has('street'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('street') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                            <label for="dob" class="col-md-4 control-label">Data di nascita</label>

                            <div class="col-md-6">
                                <input id="dob" type="text" pattern="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d" class="form-control" name="dob" placeholder="GG/MM/AAAA" value="{{ old('dob') }}">

                                @if ($errors->has('dob'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class="col-md-4 control-label">Tipo di investitore</label>

                            <div class="col-md-6">
                                <select class="form-control" name="type" id="type">
                                  <option value="0" {{ (old('type') == 0) ? ('selected') : ('') }}>straordinario (HMWI, Sovereign: minimo 7.500.000€)</option>
                                  <option value="1" {{ (old('type') == 1) ? ('selected') : ('') }}>istituzionali (minimo 500.000€)</option>
                                  <option value="2" {{ (old('type') == 2) ? ('selected') : ('') }}>professionali (minimo 125.000€)</option>
                                  <option value="3" {{ (old('type') == 3) ? ('selected') : ('') }}>qualificati (minimo 75.000€)</option>
                                  <option value="4" {{ (old('type') == 4) ? ('selected') : ('') }}>esperti (minimo 10.000€)</option>
                                </select>

                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('cellphone') ? ' has-error' : '' }}">
                            <label for="cellphone" class="col-md-4 control-label">Indirizzo</label>

                            <div class="col-md-6">
                                <input id="cellphone" type="text" class="form-control" name="cellphone" value="{{ old('cellphone') }}">

                                @if ($errors->has('cellphone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cellphone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      </div>
                        <div class="form-group">
                            <div class="col-md-12" style="text-align: center;">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
